//
//  LoginViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 23/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD
class LoginViewController: UIViewController,dataReceivedFromServerDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            loginToYourHalagramAccountLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: loginToYourHalagramAccountLbl)
            loginLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: loginLbl)
            languageBtn.titleLabel?.font.fontChange(font: "AvenirArabic-Heavy", lable:languageBtn.titleLabel! )
//            emailTxtField.font?.fontChange(font: "AvenirArabic-Medium", lable: emailTxtField.tex)
//            passwordTxtField.font?.fontChange(font: "AvenirArabic-Medium", lable: passwordTxtField)
            emailTxtField.textAlignment = .right
            passwordTxtField.textAlignment = .right
            
            if let font = UIFont(name: "AvenirArabic-Medium", size: 17.0) {
                emailTxtField.attributedPlaceholder = NSAttributedString(
                    string: "البريد الإلكتروني",
                    attributes: [
                        NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: font
                    ])
                passwordTxtField.attributedPlaceholder = NSAttributedString(
                    string: "كلمة المرور",
                    attributes: [
                        NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: font
                    ])
            }
            

        }
        else{
            loginToYourHalagramAccountLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
            loginLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginLbl)
            if let font = UIFont(name: "DiodrumRounded-Medium", size: 17.0) {
                emailTxtField.attributedPlaceholder = NSAttributedString(
                    string: "Email",
                    attributes: [
                        NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: font
                    ])
                passwordTxtField.attributedPlaceholder = NSAttributedString(
                    string: "Password",
                    attributes: [
                        NSAttributedString.Key.foregroundColor: UIColor.white,
                        NSAttributedString.Key.font: font
                    ])
            }
            languageBtn.titleLabel?.font.fontChange(font: "AvenirArabic-Heavy", lable:languageBtn.titleLabel! )
            emailTxtField.textAlignment = .left
            passwordTxtField.textAlignment = .left
        }
        DispatchQueue.main.async {
            self.loginToYourHalagramAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login In To Your Halagram Account", comment: "")
            self.loginLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Login", comment: "")
            self.emailTxtField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Email", comment: "")
            self.passwordTxtField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Password", comment: "")
            
            self.languageBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "English", comment: ""), for: .normal)
           
        }
        
         navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
 
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var loginToYourHalagramAccountLbl: UILabel!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var loginView: UIView!
    
     var reachability = try? Reachability()
     var requestServer = RequestToServer()
     var deviceID = ""
     var activityLoader : MBProgressHUD? = nil
     var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAllViews()
        
        emailTxtField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordTxtField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])

        
       

        

        
        
        deviceID = UIDevice.current.identifierForVendor!.uuidString
        print(deviceID)
        
         requestServer.delegate = self
        
        NotificationCenter.default.addObserver(
                              self,
                              selector: #selector(reachabilityChanged),
                              name: Notification.Name.reachabilityChanged,
                              object: reachability
               )
               do{
                   try reachability?.startNotifier()
               }catch{
                   print("could not respond")
               }
 
        
    }
    
    
    
   func dataReceivedFromServer(data: NSDictionary, url: URL) {
           if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
               
               UserDefaults.standard.set(true, forKey: "login")
               
               let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
               let token  =  temporaryArray.value(forKey: "token")
               let name = temporaryArray.value(forKey: "nameEn")
               let nameAr  = temporaryArray.value(forKey: "nameAr")
               let image = temporaryArray.value(forKey: "image") as! String
            
               print(image)
               UserDefaults.standard.set(token, forKey: "token") //setObject
               UserDefaults.standard.set(image, forKey: "image")
               UserDefaults.standard.set(name, forKey: "nameEn")
              UserDefaults.standard.set(nameAr, forKey: "nameAr")
            
               
               getDataFromServer(myGetUrl: infoApi!)
//                scheduledTimerWithTimeInterval()
         
            
           }
           else if((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "error"{
            DispatchQueue.main.async {
                self.activityLoader?.hide(animated: true)
            }
           
            
            
            print("\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "message") as! String))")
            print("\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String))")
            
            alertMessage(message: "\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "message") as! String))", title: "\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String))")
            
            
       }
       }
       func getDataFromServer(myGetUrl : URL) {
            let url = myGetUrl
            
            //create the session object
            let session = URLSession.shared
            
            let header = [
                "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)",
                "lang":"\(LocalizationSystem.sharedInstance.getLanguage())"
            ]
            
            
            //now create the URLRequest object using the url object
            var request = URLRequest(url: url )
            
            request.allHTTPHeaderFields = header
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                        print(json)
                        if json.count > 0 {

                           if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                                       let temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
                            
                                        let isApproved = temporaryArray.value(forKey: "isApproved") as! Bool
                            UserDefaults.standard.set(temporaryArray.value(forKey: "isApproved"), forKey: "isApproved")
                            UserDefaults.standard.set(temporaryArray.value(forKey: "dashboardMsg"), forKey: "dashboardMsg")

                            
    //                        print(isApproved)
                            if ((isApproved) == true){

                                UserDefaults.standard.set(temporaryArray.value(forKey: "reactionCount"), forKey: "reactionCount")
                                UserDefaults.standard.set(temporaryArray.value(forKey: "requestCount"), forKey: "requestCount")
                                UserDefaults.standard.set(temporaryArray.value(forKey: "reviewCount"), forKey: "reviewCount")

                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                DispatchQueue.main.async {
                                    self.activityLoader?.hide(animated: true)
                                     let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                               
                                    self.navigationController?.pushViewController(vc, animated: true)
                                   
                                }
                               
                                
                                
                            }
                            else{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                DispatchQueue.main.async {
                                    self.activityLoader?.hide(animated: true)
                                     let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                                                               
                                    self.navigationController?.pushViewController(vc, animated: true)
                                   
                                }
                            }
                                       
                            
                            }
                            
                            
                            
                        }
                    
                      
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
       
    func setupAllViews(){
        DispatchQueue.main.async {
            self.emailView.cornerRadius(view: self.emailView)
            self.passwordView.cornerRadius(view: self.passwordView)
            self.loginView.makeCircular()
        }
    }
    
    @objc func reachabilityChanged(note:Notification) -> Bool {
          let reachability = note.object as! Reachability
          
          if reachability.isReachable{
              print("reachable")
            return true
          }
          else{
              print("unreachable")
            alert(message: "No internet connection", title: "")
           return false
          }
          
      }
    
    @IBAction func btnLoginTapped(_ sender: Any) {

        
//        if (emailTxtField.text==""&&passwordTxtField.text==""){
//            alert(message: "email and password are required")
//        }
         if (emailTxtField.text==""){
            
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                alert(message: "email is required")
            }
            else{
                alert(message: "يجب اضافة البريد الإلكتروني")
            }
             
        }
        else if (passwordTxtField.text==""){
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                alert(message: "password is required")
            }
            else{
                alert(message: "يجب اضافة كلمة السر")
            }
        }
        else if(emailTxtField.text != ""){
            if(isValidEmail(emailTxtField.text!)) == false{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    alert(message: "Email is invalid")
                }
                else{
                    alert(message: "البريد الإلكتروني غير صالح")
                }
            }
            else{
                if reachability!.isReachable{

                          DispatchQueue.main.async {
                              self.loadActivityIndicator()
                            let params = ["email":"\(self.emailTxtField.text!)","password":"\(self.passwordTxtField.text!)", "deviceId":"\(self.deviceID)", "deviceType":"ios"]
                              print(params)
                              
                              self.requestServer.connectToServer(myUrl: checkEventStatusUrl!, params: params as AnyObject)
                          }

                          


                     }
                      else{
                          self.alert(message: "No internet connection", title: "")
                      }
            }
          
        }
       

        
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
        
        
        
    }
    func loadActivityIndicator()
    {
if LocalizationSystem.sharedInstance.getLanguage() == "en"{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "Processing";
activityLoader?.detailsLabel.text = "Please Wait";
activityLoader?.isUserInteractionEnabled = false;
}
else{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "معالجة";
activityLoader?.detailsLabel.text = "فضلا النتظار";
activityLoader?.isUserInteractionEnabled = false;
}
       
    }

    @IBAction func englishBtnTapped(_ sender: Any) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
            
        }
        else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")

//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
//            }

           


            UserDefaults.standard.setValue("arabic", forKey: "languageSetting")
            
        }
        
        
        let window = self.view.superview
        self.view.removeFromSuperview()
        window?.addSubview(self.view)
        
        viewWillAppear(true)
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
}
extension UIView{
    func makeCircular() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.clipsToBounds = true
    }
    func cornerRadius(view:UIView)  {
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
    }
}
extension UIViewController {
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

    self.present(alertController, animated: true, completion: nil)
    let when = DispatchTime.now() + 2
    DispatchQueue.main.asyncAfter(deadline: when){
      
      alertController.dismiss(animated: true, completion: nil)
    }
  }
}
extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }

}
extension UIViewController {
  func alertMessage(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    DispatchQueue.main.async {
        self.present(alertController, animated: true, completion: nil)
    }
    
  }
}
extension UIFont{
    func fontChange(font: String, lable: UILabel) {
        lable.font = UIFont(name:"\(font)",size:17)

      }
    func fontChanges(font: String, lable: UILabel) {
        lable.font = UIFont(name:"\(font)",size:14)

      }
    func fontChangesSize(font: String, lable: UILabel, size: Float) {
        lable.font = UIFont(name:"\(font)",size:CGFloat(size))

      }
    }
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
