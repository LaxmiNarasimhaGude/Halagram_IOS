//
//  UploadYourVideoViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 29/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices
import Alamofire
import Photos
import AVFoundation
import MBProgressHUD

class UploadYourVideoViewController: UIViewController,dataReceivedFromServerDelegate {
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
            if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                
//                UserDefaults.standard.set(true, forKey: "login")
                
                let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! String)
                print(temporaryArray)
                if temporaryArray != ""{
                    UserDefaults.standard.setValue(temporaryArray, forKey: "welcomevideo")
                    DispatchQueue.main.async { [self] in
                        self.recordView.isHidden = true
//                        self.cameraView.isHidden = true
//                        self.videoView.isHidden = false
                        playBtn.imageView?.isHidden = true
                        
                        self.avPlayer = AVPlayer()
                        var avPlayerLayer: AVPlayerLayer!
            
            
            
            
            
                        var videoURL: URL!
                        
                        let videoString = temporaryArray
                        print(videoString)
                        videoURL = URL(string: videoString)
                        print(videoURL)
                       
                        avPlayerLayer = AVPlayerLayer(player: avPlayer)
                                avPlayerLayer.frame = videoView.bounds
                                avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                                 videoView.layer.sublayers = nil
                                videoView.layer.insertSublayer(avPlayerLayer, at: 0)
            
                                view.layoutIfNeeded()
            
                                let playerItem = AVPlayerItem(url: videoURL as URL)
                                avPlayer.replaceCurrentItem(with: playerItem)
            
                                avPlayer.play()
//                                        activityLoader?.hide(animated: true)
//                                activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
                        DispatchQueue.main.async {
                           activityLoader?.hide(animated: true)
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.updateYourWelcomeView.isHidden = true
                        self.recordView.isHidden = false
                        self.createSessionFirst()
                    }
                }
//                {
                    
                    
//                }
                DispatchQueue.main.async {
                   activityLoader?.hide(animated: true)
                }
                
            
        //        playBtn.isEnabled = false
          
             
            }
            else if((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "error"{
            
                DispatchQueue.main.async {
                   activityLoader?.hide(animated: true)
                }
             
             
//             print("\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "message") as! String))")
//             print("\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String))")
             
             alertMessage(message: "\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "message") as! String))", title: "\(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String))")
             
             
        }
        }


    @IBOutlet weak var yourLbl: UILabel!
    @IBOutlet weak var updateYourWelcomeVideoLbl: UILabel!
    @IBOutlet weak var updateYourWelcomeView: UIView!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var uploadLbl: UILabel!
    @IBOutlet weak var redoLbl: UILabel!
    @IBOutlet weak var recordYourVideoLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var recordVideoImage: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var stopView: UIView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var tipsLbl: UILabel!
    @IBOutlet weak var weRecommendLbl: UILabel!
    @IBOutlet weak var makeSureYourCameraLbl: UILabel!
    @IBOutlet weak var makeSuretoGiveLbl: UILabel!
    @IBOutlet weak var makeSureYourInternetLbl: UILabel!
    
    @IBOutlet weak var counterLbl: UILabel!
    
    
    
    var session: AVCaptureSession?
    var userreponsevideoData = NSData()
    var userreponsethumbimageData = NSData()
    var upload = true
    var reachability = try? Reachability()
    var assetReader: AVAssetReader?
    let bitrate: NSNumber = NSNumber(value: 1250000)
    var assetWriter: AVAssetWriter!
    var save = true
    var isRecording = true
    var avPlayer = AVPlayer()
    var requestServer = RequestToServer()
    var isrecording = true
    var timeMin = 0
    var timeSec = 0
    weak var timer: Timer?
    var counter = 5
    var lbltimer:Timer?
    
    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self)
        
        avPlayer.pause()
        session?.stopRunning()
    }
    override func viewWillAppear(_ animated: Bool) {
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
        super.viewWillAppear(animated)
        self.recordView.isHidden = false
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            headerLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: headerLbl)
            uploadLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: uploadLbl)
            redoLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: redoLbl)
            tipsLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: tipsLbl)
            weRecommendLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: weRecommendLbl)
            makeSureYourCameraLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSureYourCameraLbl)
            makeSuretoGiveLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSuretoGiveLbl)
            makeSureYourInternetLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSureYourInternetLbl)
            recordYourVideoLbl.font.fontChange(font: "AvenirArabic-Medium", lable: recordYourVideoLbl)
            updateYourWelcomeVideoLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: updateYourWelcomeVideoLbl, size: 15.0)
            noteLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: noteLbl)
           
            

        }
        else{
            headerLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: headerLbl)
            noteLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: noteLbl)
            redoLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: redoLbl)
            recordYourVideoLbl.font?.fontChange(font: "DiodrumRounded-Medium", lable: recordYourVideoLbl)
            uploadLbl.font?.fontChange(font: "DiodrumRounded-Semibold", lable: uploadLbl)
            tipsLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: tipsLbl)
            weRecommendLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: weRecommendLbl)
            makeSureYourCameraLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSureYourCameraLbl)
            makeSuretoGiveLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSuretoGiveLbl)
            makeSureYourInternetLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSureYourInternetLbl)
            updateYourWelcomeVideoLbl.font.fontChangesSize(font: "DiodrumRounded-Medium", lable: updateYourWelcomeVideoLbl, size: 15.0)
           
        }
        
        
        
        
        
        
        
        
        
        
        
        
//        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
//           if granted == true {
//              print("granted")
//           } else {
//            DispatchQueue.main.async {
//                print("notgranted")
//            }
//
//           }
//       })
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
       print("hello")
        }
        else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
               if granted == true {
                  print("granted")
               }
               else {
                DispatchQueue.main.async { [self] in
                    print("notgranted")
//                    avPlayer.pause()
//                    self.navigationController?.popViewController(animated: true)
                }

               }
           })
        }
        
        
        DispatchQueue.main.async {
            self.recordYourVideoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Record Your Video", comment: "")
            self.redoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Redo", comment: "")
            self.uploadLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Upload", comment: "")
            self.headerLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Upload Welcome Video Header", comment: "")
            self.noteLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Note:", comment: "")
            self.tipsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tips to make sure you shoot the best video.", comment: "")
            self.weRecommendLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "1. We recommend that your device is in potrait mode while taking the video.", comment: "")
            self.makeSureYourCameraLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "2. Make sure your camera isn't being used by another application while you are taking the video.", comment: "")
            self.makeSuretoGiveLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "3. Make sure to give permission to the application or web browser to access the camera.", comment: "")
            self.makeSureYourInternetLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "4. Make sure your internet connection is stable while uploading and that you double check your video before sharing.", comment: "")
            self.updateYourWelcomeVideoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Upload Welcome Video Btn", comment: "").uppercased()
//            self.englishBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "English", comment: ""), for: .normal)
           
        }

        
        
        
        
        totalLbl.makeCircular()
        
        self.cameraView.isHidden = true
        self.videoView.isHidden = false
        if reachability!.isReachable{
            DispatchQueue.main.async {
                  self.requestServer.getDataFromServer(myGetUrl: introVideoUrl!)
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.loadActivityIndicator(text: "Processing")
                }
                else{
                    self.loadActivityIndicator(text: "معالجة")
                }
                
            }
                 
               }
               else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
        
        
        
        let totalCount = UserDefaults.standard.value(forKey: "totalCount") as! Int
        if totalCount>0{
            self.totalLbl.isHidden = false
            self.totalLbl.text = "\(totalCount)"
        }
        else{
            self.totalLbl.isHidden = true
        }
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    func createSessionFirst() {
        
        DispatchQueue.main.async { [self] in
            
        
        let audioDevice = AVCaptureDevice.default(for: .audio)
        var audioInput: AVCaptureDeviceInput? = nil

          var input: AVCaptureDeviceInput?
        
//        var audioInput = AVCaptureDevice.default(for: AVMediaType.audio)
        
          let  movieFileOutput = AVCaptureMovieFileOutput()
          var prevLayer: AVCaptureVideoPreviewLayer?
          prevLayer?.frame.size = cameraView.frame.size
          session = AVCaptureSession()
          let error: NSError? = nil
        do { input = try AVCaptureDeviceInput(device: self.cameraWithPosition(position: .front)!)
            if let audioDevice = audioDevice {
                    audioInput = try AVCaptureDeviceInput(device: audioDevice)
                }
            
//            audioInput = try AVCaptureDeviceInput(device: audioInput!)



        } catch {return}
          if error == nil {
            session?.addInput(input!)
            if let audioInput = audioInput {
                session?.addInput(audioInput)
               
            }
            
            
            
//            session?.addInput(audioInput!)
//            try self.session!.addInput(AVCaptureDeviceInput(device: audioInput!))

            
          } else {
              print("camera input error: \(error)")
          }
        prevLayer = AVCaptureVideoPreviewLayer(session: session!)
          prevLayer?.frame.size = cameraView.frame.size
        prevLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        prevLayer?.connection!.videoOrientation = .portrait
        cameraView.layer.addSublayer(prevLayer!)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//          let  filemainurl = NSURL(string: ("\(documentsURL.URLByAppendingPathComponent("temp")!)" + ".mov"))

        let  filemainurl = documentsURL.appendingPathComponent("temp.mov")
        

          let maxDuration: CMTime = CMTimeMake(600, 10)
          movieFileOutput.maxRecordedDuration = maxDuration
          movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024
          if self.session!.canAddOutput(movieFileOutput) {
              self.session!.addOutput(movieFileOutput)
            self.session?.sessionPreset =  .medium
          }
          session?.startRunning()
//        movieFileOutput.startRecording(to: filemainurl, recordingDelegate: self)

      }
    }
    override func viewDidDisappear(_ animated: Bool) {
        resetTimerToZero()
    }
    fileprivate func startTimer(){
        
        // if you want the timer to reset to 0 every time the user presses record you can uncomment out either of these 2 lines

        // timeSec = 0
        // timeMin = 0

        // If you don't use the 2 lines above then the timer will continue from whatever time it was stopped at
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        yourLbl.text = timeNow

        stopTimer() // stop it at it's current time before starting it again
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
                    self?.timerTick()
                }
    }
    @objc fileprivate func timerTick(){
         timeSec += 1
            
         if timeSec == 60{
             timeSec = 0
             timeMin += 1
         }
            
         let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
            
         yourLbl.text = timeNow
    }

    // resets both vars back to 0 and when the timer starts again it will start at 0
    @objc fileprivate func resetTimerToZero(){
         timeSec = 0
         timeMin = 0
         stopTimer()
    }

    // if you need to reset the timer to 0 and yourLabel.txt back to 00:00
    @objc fileprivate func resetTimerAndLabel(){

         resetTimerToZero()
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
    }

    // stops the timer at it's current time
    @objc fileprivate func stopTimer(){

         timer?.invalidate()
    }
    @objc func update() {
        if(counter > 0) {
            avPlayer.pause()
            counterLbl.isHidden = false
            let ncounter = counter-1
                counter = ncounter
            counterLbl.text = "Recording will start in \n\(ncounter)"
        }
        else{
            recordView.isHidden = false
            lbltimer?.invalidate()
            counterLbl.isHidden = true
            avPlayer.pause()
            cameraView.isHidden = false
            counterLbl.isHidden = true
            videoView.isHidden = true
            createSession()

            DispatchQueue.main.async {
                self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
                self.playBtn.imageView?.isHidden = true
                self.save = false
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        UserDefaults.standard.set(true, forKey: "slidemenu")
        updateYourWelcomeView.cornerRadius(view: updateYourWelcomeView)
        
        self.requestServer.delegate = self
        
//        playBtn.isEnabled = false
        
//        UIApplication.topview
        
        
        self.videoView.isHidden = true
        self.counterLbl.isHidden = true

        DispatchQueue.main.async {
            
            self.cameraView.cornerRadius(view: self.cameraView)
            self.startView.cornerRadius(view: self.startView)
            self.stopView.cornerRadius(view: self.stopView)
            
            self.stopView.isHidden = true
            self.startView.isHidden = true
            self.recordView.isHidden = false
            self.cameraView.isUserInteractionEnabled = false
            self.videoView.isUserInteractionEnabled = false
            
        }
    }

    
    
    @IBAction func stopBtnTapped(_ sender: Any) {
        session?.stopRunning()
        uploadBtn.isHidden = false
        retakeBtn.isHidden = false
        
        
        
        
        
        
    }
    @IBAction func menuBtnTapped(_ sender: Any) {
        if (UserDefaults.standard.bool(forKey: "slidemenu")){
            UserDefaults.standard.set(false, forKey: "slidemenu")
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
//                UserDefaults.standard.set(false, forKey: "slidemenu")
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
               
            }
            else{
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 60, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
            }
           
           
            
            
            

           
        }
       
       
        
        
    }
    
    func compressFile(_ urlToCompress: URL, completion:@escaping (URL)->Void) {
        
        var audioFinished = false
        var videoFinished = false
        
        let asset = AVAsset(url: urlToCompress)
        
        //create asset reader
        do {
            assetReader = try AVAssetReader(asset: asset)
        } catch {
            assetReader = nil
        }
        
        guard let reader = assetReader else {
            print("Could not iniitalize asset reader probably failed its try catch")
            // show user error message/alert
            return
        }
        
        guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else { return }
        let videoReaderSettings: [String:Any] = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32ARGB]
        
        let assetReaderVideoOutput = AVAssetReaderTrackOutput(track: videoTrack, outputSettings: videoReaderSettings)
        
        var assetReaderAudioOutput: AVAssetReaderTrackOutput?
        if let audioTrack = asset.tracks(withMediaType: AVMediaType.audio).first {
            
            let audioReaderSettings: [String : Any] = [
                AVFormatIDKey: kAudioFormatLinearPCM,
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2
            ]
            
            assetReaderAudioOutput = AVAssetReaderTrackOutput(track: audioTrack, outputSettings: audioReaderSettings)
            
            if reader.canAdd(assetReaderAudioOutput!) {
                reader.add(assetReaderAudioOutput!)
            } else {
                print("Couldn't add audio output reader")
                // show user error message/alert
                return
            }
        }
        
        if reader.canAdd(assetReaderVideoOutput) {
            reader.add(assetReaderVideoOutput)
        } else {
            print("Couldn't add video output reader")
            // show user error message/alert
            return
        }
        
        let videoSettings:[String:Any] = [
            AVVideoCompressionPropertiesKey: [AVVideoAverageBitRateKey: self.bitrate],
            AVVideoCodecKey: AVVideoCodecType.h264,
            AVVideoHeightKey: videoTrack.naturalSize.height,
            AVVideoWidthKey: videoTrack.naturalSize.width,
            AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill
        ]
        
        let audioSettings: [String:Any] = [AVFormatIDKey : kAudioFormatMPEG4AAC,
                                           AVNumberOfChannelsKey : 2,
                                           AVSampleRateKey : 44100.0,
                                           AVEncoderBitRateKey: 128000
        ]
        
        let audioInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioSettings)
        let videoInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: videoSettings)
        videoInput.transform = videoTrack.preferredTransform
        
        let videoInputQueue = DispatchQueue(label: "videoQueue")
        let audioInputQueue = DispatchQueue(label: "audioQueue")
        
        do {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
            let date = Date()
            let tempDir = NSTemporaryDirectory()
            let outputPath = "\(tempDir)/\(formatter.string(from: date)).mp4"
            let outputURL = URL(fileURLWithPath: outputPath)
            
            assetWriter = try AVAssetWriter(outputURL: outputURL, fileType: AVFileType.mp4)
            
        } catch {
            assetWriter = nil
        }
        guard let writer = assetWriter else {
            print("assetWriter was nil")
            // show user error message/alert
            return
        }
        
        writer.shouldOptimizeForNetworkUse = true
        writer.add(videoInput)
        writer.add(audioInput)
        
        writer.startWriting()
        reader.startReading()
        writer.startSession(atSourceTime: kCMTimeZero)
        
        let closeWriter:()->Void = {
            if (audioFinished && videoFinished) {
                self.assetWriter?.finishWriting(completionHandler: { [weak self] in
                    
                    if let assetWriter = self?.assetWriter {
                        do {
                            let data = try Data(contentsOf: assetWriter.outputURL)
                            print("compressFile -file size after compression: \(Double(data.count / 1048576)) mb")
                        } catch let err as NSError {
                            print("compressFile Error: \(err.localizedDescription)")
                        }
                    }
                    
                    completion((self?.assetWriter?.outputURL)!)
                })
                
                self.assetReader?.cancelReading()
            }
        }
        
        audioInput.requestMediaDataWhenReady(on: audioInputQueue) {
            while(audioInput.isReadyForMoreMediaData) {
                if let cmSampleBuffer = assetReaderAudioOutput?.copyNextSampleBuffer() {
                    
                    audioInput.append(cmSampleBuffer)
                    
                } else {
                    audioInput.markAsFinished()
                    DispatchQueue.main.async {
                        audioFinished = true
                        closeWriter()
                    }
                    break;
                }
            }
        }
        
        videoInput.requestMediaDataWhenReady(on: videoInputQueue) {
            // request data here
            while(videoInput.isReadyForMoreMediaData) {
                if let cmSampleBuffer = assetReaderVideoOutput.copyNextSampleBuffer() {
                    
                    videoInput.append(cmSampleBuffer)
                    
                } else {
                    videoInput.markAsFinished()
                    DispatchQueue.main.async {
                        videoFinished = true
                        closeWriter()
                    }
                    break;
                }
            }
        }
    }
    
    
    
    
    
   
    
    func createSession() {
        
        startTimer()

        let audioDevice = AVCaptureDevice.default(for: .audio)
        var audioInput: AVCaptureDeviceInput? = nil
        

          var input: AVCaptureDeviceInput?
        

        
          let  movieFileOutput = AVCaptureMovieFileOutput()
          var prevLayer: AVCaptureVideoPreviewLayer?
          prevLayer?.frame.size = cameraView.frame.size
          session = AVCaptureSession()
          let error: NSError? = nil
        do { input = try AVCaptureDeviceInput(device: self.cameraWithPosition(position: .front)!)
            
            if let audioDevice = audioDevice {
                    audioInput = try AVCaptureDeviceInput(device: audioDevice)
                }
            




        } catch {return}
          if error == nil {
            session?.addInput(input!)
            if let audioInput = audioInput {
                session?.addInput(audioInput)
            }

            
          } else {
              print("camera input error: \(error)")
          }
        prevLayer = AVCaptureVideoPreviewLayer(session: session!)
          prevLayer?.frame.size = cameraView.frame.size
        prevLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        prevLayer?.connection!.videoOrientation = .portrait
        cameraView.layer.addSublayer(prevLayer!)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//          let  filemainurl = NSURL(string: ("\(documentsURL.URLByAppendingPathComponent("temp")!)" + ".mov"))

        let  filemainurl = documentsURL.appendingPathComponent("temp.mov")
        

          let maxDuration: CMTime = CMTimeMake(6000, 10)
          movieFileOutput.maxRecordedDuration = maxDuration
          movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024
          if self.session!.canAddOutput(movieFileOutput) {
              self.session!.addOutput(movieFileOutput)
            self.session?.sessionPreset =  .medium
          }
          session?.startRunning()
        movieFileOutput.startRecording(to: filemainurl, recordingDelegate: self)

      }
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
          for device in devices {
              if device.position == position {
                  return device as? AVCaptureDevice
              }
          }
          return nil
      }
    @IBAction func recordTapped(_ sender: Any) {
        
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [self] (granted: Bool) -> Void in
           if granted == true {
            DispatchQueue.main.async {
                print("granted")
              isRecording = false
                recordView.isHidden = false
                updateYourWelcomeView.isHidden = true

                  
                  
                  if save == true{
                    
                    self.lbltimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(update), userInfo: nil, repeats: true)
                    recordView.isHidden = true
//                      avPlayer.pause()
//                      cameraView.isHidden = false
//                      videoView.isHidden = true
//                      createSession()
//
//                      DispatchQueue.main.async {
//                          self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
//                          self.playBtn.imageView?.isHidden = true
//                          self.save = false
//                      }
                     
                  }
                  else{
      //                playBtn.isEnabled = true
                    stopTimer()
                      self.session?.stopRunning()
                    
                      self.recordVideoImage.isHidden = true
                      self.uploadBtn.isHidden = false
                      self.retakeBtn.isHidden = false
                      self.startView.isHidden = false
                      self.stopView.isHidden = false
                      
                      cameraView.isHidden = true
                      videoView.isHidden = false
                      
                       avPlayer = AVPlayer()
                      var avPlayerLayer: AVPlayerLayer!
                      
                      
                      var path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                      var folder: String = path[0] as! String
                         NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
                      let wordToRemove = "Preferences"


                      if let range = folder.range(of: wordToRemove) {
                          folder.removeSubrange(range)
                      }
                     folder += "/temp.mov"
                      
                      var str = "file//"

                      let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
                      folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
                      
                      print(folder)
                      
                      
                      var videoURL: URL!
                      videoURL = URL(string: folder)
                      avPlayerLayer = AVPlayerLayer(player: avPlayer)
                              avPlayerLayer.frame = videoView.bounds
                              avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                              videoView.layer.sublayers = nil
                              videoView.layer.insertSublayer(avPlayerLayer, at: 0)
                          
                      videoView.layoutIfNeeded()
                          
                              let playerItem = AVPlayerItem(url: videoURL as URL)
                              avPlayer.replaceCurrentItem(with: playerItem)
                          
                              avPlayer.play()
                      
                  }
            }
            
           }
           else {
            DispatchQueue.main.async { [self] in
                print("notgranted")
                    avPlayer.pause()
                
                    self.navigationController?.popViewController(animated: true)
            }

           }
       })
        
        
        
        
        

        
    }
    
    
    func writeToFile(urlString: String)  {

      guard let videoUrl = URL(string: urlString) else {
          return
      }

      do {

          let videoData = try Data(contentsOf: videoUrl)

          let fm = FileManager.default

          guard let docUrl = fm.urls(for: .documentDirectory, in: .userDomainMask).first else {
              print("Unable to reach the documents folder")
              return
          }

          let localUrl = docUrl.appendingPathComponent("test.mp4")

          try videoData.write(to: localUrl)

      } catch  {
          print("could not save data")
      }
   }
    
    @IBAction func retake(_ sender: Any) {
        resetTimerToZero()
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
        avPlayer.pause()
        DispatchQueue.main.async {
            self.recordVideoImage.isHidden = false
            self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
            
        }
        cameraView.isHidden = false
        videoView.isHidden = true
        retakeBtn.isHidden = true
        uploadBtn.isHidden = true
        stopView.isHidden = true
        startView.isHidden = true
        videoView.layer.sublayers = nil
        createSession()

        
    }
    
    
    func loadActivityIndicator(text:String)
                   {
                       activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//                       activityLoader?.label.text = "Loading";
                       activityLoader?.detailsLabel.text = text;
                       activityLoader?.isUserInteractionEnabled = false;
                   }
    
    @IBAction func playVideo(_ sender: Any) {
        
       
        


        
        if isRecording == true{
                    DispatchQueue.main.async { [self] in
//                        self.cameraView.isHidden = true
//                        self.videoView.isHidden = false
                        playBtn.imageView?.isHidden = true
                        
                        self.avPlayer = AVPlayer()
                        var avPlayerLayer: AVPlayerLayer!
            
            
            
            
            
                        var videoURL: URL!
                        
                        let videoString = (UserDefaults.standard.value(forKey: "welcomevideo")) as! String
                        print(videoString)
                        videoURL = URL(string: videoString)
                        print(videoURL)
                       
                        avPlayerLayer = AVPlayerLayer(player: avPlayer)
                                avPlayerLayer.frame = videoView.bounds
                                avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                                 videoView.layer.sublayers = nil
                                videoView.layer.insertSublayer(avPlayerLayer, at: 0)
            
                                view.layoutIfNeeded()
            
                                let playerItem = AVPlayerItem(url: videoURL as URL)
                                avPlayer.replaceCurrentItem(with: playerItem)
            
                                avPlayer.play()
                                activityLoader?.hide(animated: true)
            //                           isRecording = false
                    }
            
        }
        else{
            DispatchQueue.main.async { [self] in
                playBtn.imageView?.isHidden = true
                
                cameraView.isHidden = true
                videoView.isHidden = false
       
        
                avPlayer = AVPlayer()
                var avPlayerLayer: AVPlayerLayer!
        
        
                var path: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [AnyObject]
                var folder: String = path[0] as! String
                   NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
                let wordToRemove = "Preferences"
        
        
                if let range = folder.range(of: wordToRemove) {
                    folder.removeSubrange(range)
                }
               folder += "/temp.mov"
        
                var str = "file//"
        
                let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
                folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
        
                print(folder)
        
        
                var videoURL: URL!
                videoURL = URL(string: folder)
                avPlayerLayer = AVPlayerLayer(player: avPlayer)
                        avPlayerLayer.frame = videoView.bounds
                        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                         videoView.layer.sublayers = nil
                        videoView.layer.insertSublayer(avPlayerLayer, at: 0)
        
                        view.layoutIfNeeded()
        
                        let playerItem = AVPlayerItem(url: videoURL as URL)
                        avPlayer.replaceCurrentItem(with: playerItem)
        
                        avPlayer.play()
            }
            
              
            //        playBtn.isEnabled = false
        }
        
       
        
        
        
        
        
        

        
    }
    
    @IBAction func upload(_ sender: Any) {
        self.view.isUserInteractionEnabled = false

        
        if reachability!.isReachable{
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    
                    if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                        self.loadActivityIndicator(text: "Uploading will take few minutes depending on the size of the video and the internet speed")
                    }
                    else{
                        self.loadActivityIndicator(text: "سيستغرق التحميل بضع دقائق حسب حجم الفيديو وسرعة الإنترنت")
                    }
                   
//                    self.view.isUserInteractionEnabled = false

                }
                
                
                
                
                
                var path: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [AnyObject]
                var folder: String = path[0] as! String
                   NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
                let wordToRemove = "Preferences"


                if let range = folder.range(of: wordToRemove) {
                    folder.removeSubrange(range)
                }
               folder += "/temp.mov"
                
                var str = "file//"

                let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
                folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
                
                print(folder)
                
               let urlString = URL(string: folder)
                    print(urlString)
               
                self.compressFile(urlString!) { (url) in
                    print(url )
                    
                    
                                              print("\(UserDefaults.standard.string(forKey: "token")!)")
                    
                    
                                             let headers: HTTPHeaders = [
                                                      "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                                                      "cache-control": "no-cache",
                                                "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)"
                                                    ]
                                        print(headers)
                                                    do {
                    
                                                        AF.upload(
                                                                       multipartFormData: { multipartFormData in
                    
                                                                        multipartFormData.append(url, withName: "introVideo")
                    
                    
                    
                    
                                                                   },
                                                                       to: "https://halagram.me/backend/app/talent/upload", method: .post , headers: headers)
                                                            .response { resp in switch resp.result{
                    
                                                            case .success(_):
                                                                print("success")

                                                                
  let storyboard = UIStoryboard(name: "Main", bundle: nil)

  let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                             
    if LocalizationSystem.sharedInstance.getLanguage() == "en"{
    let alert = UIAlertController(title: "",message: "Your welcoming video successfully uploaded",preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok",style: UIAlertActionStyle.default,
    handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
    self.present(alert, animated: true, completion: nil)
                    }
    else{
    let alert = UIAlertController(title: "",message: "تم تحميل فيديو الترحيب بنجاح",preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "موافق",style: UIAlertActionStyle.default,
    handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
    self.present(alert, animated: true, completion: nil)
                                                                }
                                                                
    activityLoader?.hide(animated: true)
    self.view.isUserInteractionEnabled = true

//                                                                print(resp.result)
//                                                                print(resp.error)
//                                                                print(resp.data)
//                                                                print(resp.description)
            case .failure(_):
            print("fail")
                
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "Upload Failed - Please Check Internet Connection and Click on “Upload HalaGram” Again")
                }
                else{
                    self.alert(message: "لم يتم تحميل الفيديو، فضلا التأكد من الاتصال بالانترنت و انقر تحميل الهلاجرام")
                }
        activityLoader?.hide(animated: true)
        self.view.isUserInteractionEnabled = true
                                                            }
                                                                           print(resp)
                    
                    
                                                                   }
                    
                                                                                }
                                                    catch  {
                                                                                }
                                        }
                
                
                



            }
                 
               }
               else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
        
        


    }
    
    @IBAction func updateWelcomeVideoTapped(_ sender: Any) {
        
        
    }
    
    
    @IBAction func englishBtnTapped(_ sender: Any) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
            
        }
        else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")

//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
//            }

           


            UserDefaults.standard.setValue("arabic", forKey: "languageSetting")
            
        }
        
        
        let window = self.view.superview
        self.view.removeFromSuperview()
        window?.addSubview(self.view)
        
        viewWillAppear(true)
    }

    
}
extension UploadYourVideoViewController: AVCaptureFileOutputRecordingDelegate{
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print(outputFileURL)
        
//        UserDefaults.standard.setValue("\(outputFileURL)", forKey: "galleryUrl")
        UserDefaults.standard.set(outputFileURL, forKey: "galleryUrl")

        
        
//        if upload == true{
//            saveVideoToAlbum(outputFileURL, nil)
//        }
        
       
        
        

        
        

        

    }

    func requestAuthorization(completion: @escaping ()->Void) {
            if PHPhotoLibrary.authorizationStatus() == .notDetermined {
                PHPhotoLibrary.requestAuthorization { (status) in
                    DispatchQueue.main.async {
                        completion()
                    }
                }
            } else if PHPhotoLibrary.authorizationStatus() == .authorized{
                completion()
            }
        }
    func saveVideoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
            requestAuthorization {
                PHPhotoLibrary.shared().performChanges({
                    let request = PHAssetCreationRequest.forAsset()
                    request.addResource(with: .video, fileURL: outputURL, options: nil)
                }) { (result, error) in
                    DispatchQueue.main.async {
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            print("Saved successfully")
                            
                            
                        }
                        completion?(error)
                    }
                }
            }
        }

    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        print(fileURL)
    }



}
