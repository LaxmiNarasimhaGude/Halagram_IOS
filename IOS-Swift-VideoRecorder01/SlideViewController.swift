//
//  SlideViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 25/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD


 var activityLoader : MBProgressHUD? = nil

class SlideViewController: UIViewController,dataReceivedFromServerDelegate {
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
         if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
            let logOutString = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String)
                  
            if logOutString == "success"{
                (UserDefaults.standard.set(false, forKey: "login"))
                
                
                
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                DispatchQueue.main.async {
                    self.activityLoader?.hide(animated: true)
              let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

             let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                     let navigationController = UINavigationController(rootViewController: loginVC)

                    
                    
             appDel.window?.rootViewController = navigationController
                }
                
                
            }
            
            
        }
         else if(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "token expired")||(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "invalid token"){
            
            DispatchQueue.main.async {
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

                        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                                let navigationController = UINavigationController(rootViewController: loginVC)



                        appDel.window?.rootViewController = navigationController
                           }

            
         }

    }
    
    
    @IBOutlet weak var ratingAndReviewsLbl: UILabel!
    @IBOutlet weak var logoutLbl: UILabel!
    @IBOutlet weak var myOrdersLbl: UILabel!
    @IBOutlet weak var wishListLbl: UILabel!
    @IBOutlet weak var earningsLbl: UILabel!
    @IBOutlet weak var completedRequestLbl: UILabel!
    @IBOutlet weak var newRequestLbl: UILabel!
    @IBOutlet weak var welcomeVideoLbl: UILabel!
    @IBOutlet weak var myProfileLbl: UILabel!
    @IBOutlet weak var dashboardLbl: UILabel!
    @IBOutlet weak var ratingAndReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingAndReviewView: UIView!
    
    @IBOutlet weak var earningsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var earningsView: UIView!
    @IBOutlet weak var completedReqHeight: NSLayoutConstraint!
    @IBOutlet weak var completedReqView: UIView!
    @IBOutlet weak var newRequestView: UIView!
    @IBOutlet weak var newRequestViewHeight: NSLayoutConstraint!
    @IBOutlet weak var newRequestBtn: UIButton!
    
    @IBOutlet weak var userLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var ratingAndReviewCount: UILabel!
    @IBOutlet weak var completedRequestCount: UILabel!
    @IBOutlet weak var newRequestCount: UILabel!
    var reachability = try? Reachability()
    var requestServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.dashboardLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Dashboard", comment: "")
            self.myProfileLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "My Profile", comment: "")
            self.welcomeVideoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Welcome Video", comment: "")
            self.newRequestLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New Requests", comment: "")
            self.completedRequestLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Completed Requests", comment: "")
            self.earningsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Earnings", comment: "")
            self.ratingAndReviewsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Rating & Reviews", comment: "")
            self.wishListLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Wishlist", comment: "")
            self.myOrdersLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "My Orders", comment: "")
            self.logoutLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Logout", comment: "")
           
        }
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
//            halagramYourFansLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//
            myProfileLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            welcomeVideoLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            dashboardLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            newRequestLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            completedRequestLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            earningsLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            ratingAndReviewsLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            wishListLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            myOrdersLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            logoutLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
//
//            totalVisitorsHeadingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: totalVisitorsHeadingLbl)
//            totalRequestHeadingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: totalRequestHeadingLbl)
//            totalEarningsHeadingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: totalEarningsHeadingLbl)
//            converstionRateHeadingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: converstionRateHeadingLbl)
//            completionRateHeadingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: completionRateHeadingLbl)
//            viewDetailsLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: viewDetailsLbl)
//            totalVisitorsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//            totalRequestsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//            totalEarningsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//            convertionRateLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//            completionRateLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
//            youHave1RequestLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: youHave1RequestLbl, size: 20.0)
            

        }
        else{
            
            myProfileLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            welcomeVideoLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            dashboardLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            newRequestLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            completedRequestLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            earningsLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            ratingAndReviewsLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            wishListLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            myOrdersLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            logoutLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            
//            halagramYourFansLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: halagramYourFansLbl, size: 20.0)
//            totalVisitorsHeadingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: totalVisitorsHeadingLbl)
//            totalRequestHeadingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: totalRequestHeadingLbl)
//            totalEarningsHeadingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: totalEarningsHeadingLbl)
//            converstionRateHeadingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: converstionRateHeadingLbl)
//            completionRateHeadingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: completionRateHeadingLbl)
//            viewDetailsLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: viewDetailsLbl)
//            totalVisitorsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalVisitorsLbl, size: 20.0)
//            totalRequestsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalRequestsLbl, size: 20.0)
//            totalEarningsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalEarningsLbl, size: 20.0)
//            convertionRateLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: convertionRateLbl, size: 20.0)
//            completionRateLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: completionRateLbl, size: 20.0)
//            youHave1RequestLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: youHave1RequestLbl, size: 20.0)
//
//            loginToYourHalagramAccountLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
//            loginLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
//            emailTxtField.font?.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
//            passwordTxtField.font?.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
        }
        
        
        
        
        
        
        
        print((UserDefaults.standard.value(forKey: "isApproved")))
        
        if (((UserDefaults.standard.value(forKey: "isApproved")) as! Bool) == false){
//            print(UserDefaults.standard.value(forKey: "isApproved"))
            
            newRequestView.isHidden = true
            completedReqView.isHidden = true
            earningsView.isHidden = true
            
            newRequestViewHeight.constant = 0
            completedReqHeight.constant = 0
            earningsViewHeight.constant = 0
            
            
        }
        else{
            print("isapprovedtrue")
            newRequestView.isHidden = false
            completedReqView.isHidden = false
            earningsView.isHidden = false
            
            newRequestViewHeight.constant = 50
            completedReqHeight.constant = 50
            earningsViewHeight.constant = 50
        }
        if LocalizationSystem.sharedInstance.getLanguage() == "en"{
            userLbl.font = UIFont(name:"DiodrumRounded-Semibold", size:17)
            userLbl.text = ("WELCOME \(UserDefaults.standard.value(forKey: "nameEn")!)").uppercased()
        }
        else{
            userLbl.font = UIFont(name:"AvenirArabic-Heavy", size:17)
            userLbl.text = ("مرحبا بكم \(UserDefaults.standard.value(forKey: "nameAr")!)").uppercased()
        }
        
        
        userImage.layer.borderWidth = 1
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.white.cgColor
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        requestServer.delegate = self

        
        if reachability!.isReachable{
            
            
            print("\(UserDefaults.standard.value(forKey: "image")!)")

            DispatchQueue.main.async {
                let imgfilterUrl =   "\(UserDefaults.standard.value(forKey: "image")!)"

                        URLSession.shared.dataTask(with: NSURL(string: imgfilterUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                            print(imgfilterUrl)
                                       if error != nil {
                                           return
                                       }
                                       else {

                                           DispatchQueue.main.async {
                                               let image = UIImage(data: data!)
            //print(image)
                                               if (image != nil) {

                                                self.userImage.image = image

                                               }
                                               else{
           
                                               }
                                           }
                                       }
                                   }).resume()
               
                    self.activityLoader?.hide(animated: true)
               
                       
                                 }

            
            
            
            
            
            self.getDataFromServer(myGetUrl: infoApi!)
            DispatchQueue.main.async {
                           self.loadActivityIndicator()
                       }
            
        }
        
        
        
       
        
        
    }
    @IBAction func profile(_ sender: Any) {
//        UserDefaults.standard.set(true, forKey: "slidemenu")
        
        DispatchQueue.main.async {

            UIApplication.shared.open(profileUrl!)
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "My Profile"
//            self.navigationController?.pushViewController(VC, animated: true)

        }
        
    }
    
    @IBAction func completedRequest(_ sender: Any) {
        DispatchQueue.main.async {

            UIApplication.shared.open(completedRequestUrl!)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "Completed Requests"
//            self.navigationController?.pushViewController(VC, animated: true)

        }
        

        
    }
    
    @IBAction func earnings(_ sender: Any) {
        DispatchQueue.main.async {

//            guard let url = URL(string: earningsUrl) else { return }
            UIApplication.shared.open(earningsUrl!)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "Earnings"
//            self.navigationController?.pushViewController(VC, animated: true)

        }
        
    }
    @IBAction func rating(_ sender: Any) {
        DispatchQueue.main.async {
//            guard let url = URL(string: ratingsUrl) else { return }
            UIApplication.shared.open(ratingsUrl!)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "Ratings & Reviews"
//            self.navigationController?.pushViewController(VC, animated: true)

        }
        
    }
    @IBAction func wishList(_ sender: Any) {
        
        DispatchQueue.main.async {

//            guard let url = URL(string: wishListUrl) else { return }
            UIApplication.shared.open(wishListUrl!)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "Whishlist"
//            self.navigationController?.pushViewController(VC, animated: true)

        }

        
    }
    
    @IBAction func myorders(_ sender: Any) {
        
        DispatchQueue.main.async {

//            guard let url = URL(string: orderUrl) else { return }
            UIApplication.shared.open(orderUrl!)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let VC = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            VC.heading = "My Orders"
//            self.navigationController?.pushViewController(VC, animated: true)

        }

        
    }
    @IBAction func dashBoard(_ sender: Any) {
        let vc  = UIApplication.topViewController()
        UserDefaults.standard.set(true, forKey: "slidemenu")

        if (vc is DashBoardViewController){
            let viewMenuBack : UIView = self.view!

                     UIView.animate(withDuration: 0, animations: { () -> Void in
                         var frameMenu : CGRect = viewMenuBack.frame
                         frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                         viewMenuBack.frame = frameMenu
                         viewMenuBack.layoutIfNeeded()
                         viewMenuBack.backgroundColor = UIColor.clear
                     }, completion: { (finished) -> Void in
                         viewMenuBack.removeFromSuperview()

                     })
        }
        else{
            let name : UIStoryboard!
                          name = UIStoryboard(name: "Main", bundle: nil)
                          let vc = name.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
    //
    ////
    //////
                          DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                          }
      
        }
       
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "slidemenu")
        let viewMenuBack : UIView = self.view!

                 UIView.animate(withDuration: 0, animations: { () -> Void in
                     var frameMenu : CGRect = viewMenuBack.frame
                     frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                     viewMenuBack.frame = frameMenu
                     viewMenuBack.layoutIfNeeded()
                     viewMenuBack.backgroundColor = UIColor.clear
                 }, completion: { (finished) -> Void in
                     viewMenuBack.removeFromSuperview()

                 })
    }
    
    @IBAction func logOutBtnTapped(_ sender: Any) {
        
       if reachability!.isReachable{
        DispatchQueue.main.async {
            self.loadActivityIndicator()
            self.requestServer.getDataFromServer(myGetUrl: logOutUrl!)
        }
                  
              }
              else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
        
        
    }
    
    func loadActivityIndicator()
    {
if LocalizationSystem.sharedInstance.getLanguage() == "en"{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "Processing";
activityLoader?.detailsLabel.text = "Please Wait";
activityLoader?.isUserInteractionEnabled = false;
}
else{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "معالجة";
activityLoader?.detailsLabel.text = "فضلا النتظار";
activityLoader?.isUserInteractionEnabled = false;
}
       
    }
    
    @IBAction func requestBtnTapped(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "slidemenu")
        
        
        
        
        let vc  = UIApplication.topViewController()


        if (vc is NewRequestViewController){
            let viewMenuBack : UIView = self.view!

                     UIView.animate(withDuration: 0, animations: { () -> Void in
                         var frameMenu : CGRect = viewMenuBack.frame
                         frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                         viewMenuBack.frame = frameMenu
                         viewMenuBack.layoutIfNeeded()
                         viewMenuBack.backgroundColor = UIColor.clear
                     }, completion: { (finished) -> Void in
                         viewMenuBack.removeFromSuperview()

                     })
        }
        else if(vc is UploadYourVideoViewController){
            


                let name : UIStoryboard!
                              name = UIStoryboard(name: "Main", bundle: nil)
                              let vc = name.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController
//
////
//////
                              
                                navigationController?.pushViewController(vc, animated: true)
                             
//            }
            
            
        }
        else{
                        let name : UIStoryboard!
                                      name = UIStoryboard(name: "Main", bundle: nil)
                                      let vc = name.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController



                                      DispatchQueue.main.async {
                                        self.navigationController?.pushViewController(vc, animated: true)
                                      }
        }
        
            
            

        
        
    }
    
    @IBAction func welcomeVideo(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "slidemenu")
       
        
        
        let vc  = UIApplication.topViewController()

        
        

        if (vc is UploadYourVideoViewController){
            let viewMenuBack : UIView = self.view!

                     UIView.animate(withDuration: 0, animations: { () -> Void in
                         var frameMenu : CGRect = viewMenuBack.frame
                         frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                         viewMenuBack.frame = frameMenu
                         viewMenuBack.layoutIfNeeded()
                         viewMenuBack.backgroundColor = UIColor.clear
                     }, completion: { (finished) -> Void in
                         viewMenuBack.removeFromSuperview()

                     })
        }
        else if(vc is ViewController){
            
//            let tempVCA = navigationController?.viewControllers
//
//            for tempVC in tempVCA ?? [] {
//                if tempVC is ViewController {
//                    tempVC.removeFromParentViewController()
//
//
//                }
//            }
            
                            let name : UIStoryboard!
                                          name = UIStoryboard(name: "Main", bundle: nil)
                                          let vc = name.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController
           
                                          DispatchQueue.main.async {
                                            self.navigationController?.pushViewController(vc, animated: true)
                                          }
            
//            vc?.reloadInputViews()
            
            
            
//            guard let navigationController = self.navigationController else { return }
//            var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
//            navigationArray.remove(at: navigationArray.count - 1) // To remove previous UIViewController
//            self.navigationController?.viewControllers = navigationArray


//            }
            
            
        }
        else{
                        let name : UIStoryboard!
                                      name = UIStoryboard(name: "Main", bundle: nil)
                                      let vc = name.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController



                                      DispatchQueue.main.async {
                                        self.navigationController?.pushViewController(vc, animated: true)
                                      }
        }
        
            
            

        
        
    }
    func getDataFromServer(myGetUrl : URL) {
         let url = myGetUrl
         
         //create the session object
         let session = URLSession.shared
         
         let header = [
             "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)"
         ]
         
         
         //now create the URLRequest object using the url object
         var request = URLRequest(url: url )
         
         request.allHTTPHeaderFields = header
         
         //create dataTask using the session object to send data to the server
         let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
             
             guard error == nil else {
                 return
             }
             
             guard let data = data else {
                 return
             }
             
             do {
                 //create json object from data
                 if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                     print(json)
                     if json.count > 0 {

                        if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                                    let temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
                         
                            print(temporaryArray)

                                
                            DispatchQueue.main.async {
                                self.newRequestCount.text = "\(temporaryArray.value(forKey: "requestCount")!)"
                                
                                if(Int("\(temporaryArray.value(forKey: "requestCount")!)")!)>0{
                                    self.newRequestCount.isHidden = false
                                    self.newRequestBtn.isEnabled = true
                                
                                                                }
                                    else{
//                                    self.newRequestBtn.isEnabled = false
                                                                }
                                if(Int("\(temporaryArray.value(forKey: "reactionCount")!)")!)>0{
                                    self.completedRequestCount.isHidden = false
                                }
                                if(Int("\(temporaryArray.value(forKey: "reviewCount")!)")!)>0{
                                    self.ratingAndReviewCount.isHidden = false
                                }
                                
                                self.completedRequestCount.text = "\("\(temporaryArray.value(forKey: "reactionCount")!)")"
                                self.ratingAndReviewCount.text = "\("\(temporaryArray.value(forKey: "reviewCount")!)")"
                                
                            }
//                              pr
                            
//                                print("\(UserDefaults.standard.value(forKey: "requestCount")!)")
                               

//                            }
                            
                            
                             
//                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                             DispatchQueue.main.async {
                                 self.activityLoader?.hide(animated: true)
//                                  let vc = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//
//                                 self.navigationController?.pushViewController(vc, animated: true)

                             }
                            
                             
                             
//                         }
//                         else{
//
//                         }
                                    
                         
                         }
                         
                         
                         
                     }
                 
                   
                 }
             } catch let error {
                 print(error.localizedDescription)
             }
         })
         task.resume()
     }

}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
