//
//  WebViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 30/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD
class WebViewController: UIViewController,UIWebViewDelegate  {

    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var webView: UIWebView!
    var heading : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headingLbl.text = heading
        webView.delegate  = self
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        webView.backgroundColor = UIColor.white
        if heading == "My Profile"{

            webView.loadRequest(URLRequest(url: (profileUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
       else if heading == "Completed Requests"{

            webView.loadRequest(URLRequest(url: (completedRequestUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
       else if heading == "Earnings"{

            webView.loadRequest(URLRequest(url: (earningsUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
       else if heading == "Ratings & Reviews"{

            webView.loadRequest(URLRequest(url: (earningsUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
       else if heading == "Whishlist"{

            webView.loadRequest(URLRequest(url: (earningsUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
       else if heading == "My Orders"{

            webView.loadRequest(URLRequest(url: (earningsUrl as URL?)!) as URLRequest)
            loadActivityIndicator()
            webView.scalesPageToFit = true
        }
    }
    
    func loadActivityIndicator()
    {
if LocalizationSystem.sharedInstance.getLanguage() == "en"{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "Processing";
activityLoader?.detailsLabel.text = "Please Wait";
activityLoader?.isUserInteractionEnabled = false;
}
else{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "معالجة";
activityLoader?.detailsLabel.text = "فضلا النتظار";
activityLoader?.isUserInteractionEnabled = false;
}
       
    }
   
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityLoader?.hide(animated: true)
    }
    
    @IBAction func menu(_ sender: Any) {
        
        let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                       self.view.addSubview(menuVC.view)
              self.addChildViewController(menuVC)
                       menuVC.view.layoutIfNeeded()

                       menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                       UIView.animate(withDuration: 0.3, animations: { () -> Void in
                           menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                   }, completion:nil)
               
    }
    
}
