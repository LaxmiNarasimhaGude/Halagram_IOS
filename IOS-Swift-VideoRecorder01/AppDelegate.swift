//
//  AppDelegate.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Pooya Hatami on 2018-03-12.
//  Copyright © 2018 Pooya Hatami. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        

        UIApplication.shared.isIdleTimerDisabled = true

        
        
        IQKeyboardManager.shared.enable = true
       
        if isAppAlreadyLaunchedOnce(){

//                    let login = (UserDefaults.standard.value(forKey: "login"))
//
                    if ((UserDefaults.standard.value(forKey: "login") as! Bool) == true){
//                        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)

//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//
//                       self.window?.rootViewController = initialViewController
//                        self.window?.makeKeyAndVisible()

                        DispatchQueue.main.async {
                                                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                  let loginVC = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController

                                                          let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                                                                  let navigationController = UINavigationController(rootViewController: loginVC)



                                                          appDel.window?.rootViewController = navigationController
                                                             }

                    }
                    else{

                        DispatchQueue.main.async {
                             let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

                                    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                                            let navigationController = UINavigationController(rootViewController: loginVC)



                                    appDel.window?.rootViewController = navigationController
                                       }


//                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                     let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//
//                     self.window?.rootViewController = initialViewController
//                     self.window?.makeKeyAndVisible()
                    }
        }
        else{
//            let rootView: LoginViewController = LoginViewController()
//
//                                              if let window = self.window{
//                                                     window.rootViewController = rootView
//                                              }
            (UserDefaults.standard.set(false, forKey: "login"))
        }
        if(self.needsUpdate())
        {
            
            
            
            UserDefaults.standard.set("true", forKey: "isUpdateAvailable")
            
            
        }
        else
        {
            UserDefaults.standard.set("false", forKey: "isUpdateAvailable")
            
        }
        return true
    }
    func isAppAlreadyLaunchedOnce()->Bool{
        let defaults = UserDefaults.standard

        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            print("App already launched : \(isAppAlreadyLaunchedOnce)")
            return true
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            print("App launched first time")
            return false
        }
    }
    func needsUpdate() -> Bool {
        
        
        let infoDictionary = Bundle.main.infoDictionary
        let appID = infoDictionary!["CFBundleIdentifier"] as! String

        let url = URL(string: "http://itunes.apple.com/lookup?id=1556189104")

        guard let data = try? Data(contentsOf: url!) else {
            print("There is an error!")
            return false;
        }
        let lookup = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any]

        print(lookup)

        if let resultCount = lookup!["resultCount"] as? Int, resultCount == 1 {
            if let results = lookup!["results"] as? [[String:Any]] {


                if let appStoreVersion = results[0]["version"] as? String{

                    let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                    print("current version \(currentVersion as Any)")
                    print("appstoreVersion \(appStoreVersion)")

                    let separatedCurrentVersion = currentVersion?.split(separator: ".")
                    let separatedAppStoreVersion = appStoreVersion.split(separator: ".")
                    print(separatedCurrentVersion as Any)
                    print(separatedAppStoreVersion as Any)

        if(separatedCurrentVersion![0]>separatedAppStoreVersion[0]){


                        return false
                    }
    else if(separatedCurrentVersion![1]>separatedAppStoreVersion[1]){


                        return false
                    }
    else if(separatedCurrentVersion![2]>separatedAppStoreVersion[2]){


                        return false
                    }
        else if !(appStoreVersion == currentVersion) {



                        UserDefaults.standard.set("https://appstoreconnect.apple.com/apps/1556189104/appstore/ios/version/deliverable", forKey: "appStoreURL")

                        return true
                    }
                }
            }
        }
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
////        print(url)
////        print(url.host)
////        print(url.path)
////        print(url.baseURL)
////        print(url.scheme)
//
//        let urlScheme:String = url.scheme!
//        print(urlScheme)
//        let urlPath:String = url.path as! String
////        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//        if urlScheme == "welcomevideo"{
//            if (urlPath == "/welcome-video"){
//                if ((UserDefaults.standard.value(forKey: "login") as! Bool) == true){
//                    DispatchQueue.main.async {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    //        let loginVC = storyboard.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController
//
//                        let innerPage: UploadYourVideoViewController = storyboard.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController
//                        let nav = UINavigationController.init(rootViewController: innerPage)
//                          self.window?.rootViewController = nav
//
//
//                                                         }
//
//                }
//                else{
//
//                }
//            }
//
//        }
////        print(urlScheme)
//        else if(urlScheme == "requestvideo"){
//
//                if ((UserDefaults.standard.value(forKey: "login") as! Bool) == true){
//                    DispatchQueue.main.async {
////                        print(url.path)
//                        let orderId = (url.path).replacingOccurrences(of: "/", with: "")
//                        print(orderId)
//                        UserDefaults.standard.setValue(orderId, forKey: "OrderId")
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//
//                        let innerPage: ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                        let nav = UINavigationController.init(rootViewController: innerPage)
//                          self.window?.rootViewController = nav
//
//
//                                                         }
//
//                }
//                else{
//
//                }
////            }
//        }
//
//
//        self.window?.makeKeyAndVisible()
//        return true
//    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
//        print("hello")
        
        guard let url = userActivity.webpageURL else{
            return false
        }
        print(url)
        print(url.path)
        print(url.scheme)
        print(url.host)
//        print(url.para)
        url.path
        
        
//        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
//                let url = userActivity.webpageURL else {
//                return false
//            }
//            let urlString = url.absoluteString
//        print(urlString)
//
//            var queryParams: [String: String?] = [:]
//            if let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
//                let params = components.queryItems {
//                for param in params {
//                    queryParams[param.name] = param.value
//                }
//            }
//            return true
        
        
        
        

                    if (url.path == "/welcomevideo"){
                        if ((UserDefaults.standard.value(forKey: "login") as! Bool) == true){
                            DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //        let loginVC = storyboard.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController

                                let innerPage: UploadYourVideoViewController = storyboard.instantiateViewController(withIdentifier: "UploadYourVideoViewController") as! UploadYourVideoViewController
                                let nav = UINavigationController.init(rootViewController: innerPage)
                                  self.window?.rootViewController = nav


                                                                 }

                        }
                        else{

                        }
                    }
                    else{

                        if ((UserDefaults.standard.value(forKey: "login") as! Bool) == true){
                            DispatchQueue.main.async {
        //                        print(url.path)
                                let orderId = (url.path).replacingOccurrences(of: "/requestvideo/", with: "")
                                print(orderId)
                                UserDefaults.standard.setValue(orderId, forKey: "orderId")

                                print(UserDefaults.standard.value(forKey: "orderId")!)


                    let storyboard = UIStoryboard(name: "Main", bundle: nil)


                                let innerPage: ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav = UINavigationController.init(rootViewController: innerPage)
                                  self.window?.rootViewController = nav


                                                                 }

                        }
                        else{

                        }
                    }
        //            }
//                }

        
        
        return true
    }
}

