//
//  Webservices.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 25/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import Foundation
let checkEventStatusUrl = URL(string: "https://halagram.me/backend/app/auth/verify")
let infoApi = URL(string:"https://halagram.me/backend/app/talent/info")
let counterUrl = URL(string:"https://halagram.me/backend/app/talent/counter")
let logOutUrl = URL(string:"https://halagram.me/backend/app/auth/logout")
let detailsUrl = URL(string:"https://halagram.me/backend/app/request/all")
let viewDetailsUrl = URL(string:"https://halagram.me/backend/app/request/details")
let profileUrl = URL(string:"https://halagram.me/my-profile")
let completedRequestUrl =  URL(string:"https://halagram.me/completed-request")
let earningsUrl = URL(string:"https://halagram.me/earnings")
let ratingsUrl = URL(string:"https://halagram.me/ratings-and-reviews")
let wishListUrl = URL(string:"https://halagram.me/wishlist")
let orderUrl = URL(string:"https://halagram.me/orders")
let viewdetails = URL(string: "https://halagram.me/dashboard")
let introVideoUrl = URL(string: "https://halagram.me/backend/app/talent/introVideo")
let viewDetailsTesting = URL(string:"https://halagram.me/backend/app/request/status")
