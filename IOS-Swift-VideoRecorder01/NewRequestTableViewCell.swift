//
//  NewRequestTableViewCell.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 01/02/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit

class NewRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var daysRemainingLbl: UILabel!
    @IBOutlet weak var daysRemainingView: UIView!
    @IBOutlet weak var viewDetailsView: UIView!
    @IBOutlet weak var dateOfRequestLbl: UILabel!
    @IBOutlet weak var halagramOccasionLbl: UILabel!
    @IBOutlet weak var requestedByLbl: UILabel!
    
    
    @IBOutlet weak var viewDetailsLbl: UILabel!
    @IBOutlet weak var halagramOccasionHeadingLbl: UILabel!
    @IBOutlet weak var requestedByHeadingLbl: UILabel!
    @IBOutlet weak var dateOfRequestHeadingLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        daysRemainingView.cornerRadius(view: daysRemainingView)
        viewDetailsView.cornerRadius(view: viewDetailsView)
        
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{

            requestedByHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: requestedByHeadingLbl)
            halagramOccasionHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: halagramOccasionHeadingLbl)
            dateOfRequestHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: dateOfRequestHeadingLbl)
            requestedByLbl.font.fontChange(font: "AvenirArabic-Medium", lable: requestedByLbl)
            halagramOccasionLbl.font.fontChange(font: "AvenirArabic-Medium", lable: halagramOccasionLbl)
            dateOfRequestLbl.font.fontChange(font: "AvenirArabic-Medium", lable: dateOfRequestLbl)
            

            viewDetailsLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: viewDetailsLbl)
            daysRemainingLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: daysRemainingLbl)

            

        }
        else{
            
//            halagramYourFansLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: halagramYourFansLbl)
            requestedByHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: requestedByHeadingLbl)
            halagramOccasionHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccasionHeadingLbl)
            dateOfRequestHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequestHeadingLbl)
            requestedByLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: requestedByLbl)
            halagramOccasionLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccasionLbl)
            dateOfRequestLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequestLbl)
            viewDetailsLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: viewDetailsLbl)
            daysRemainingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: daysRemainingLbl)

        }
        
        
        
        
        
        
        
        DispatchQueue.main.async {
            self.viewDetailsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "View Details", comment: "")
            self.halagramOccasionHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Halagram Occassion", comment: "")
            self.requestedByHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Requested By", comment: "")
            self.dateOfRequestHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Date of Request", comment: "")

           
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
