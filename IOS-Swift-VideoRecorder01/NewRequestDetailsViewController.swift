//
//  NewRequestDetailsViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 27/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD
class NewRequestDetailsViewController: UIViewController,dataReceivedFromServerDelegate  {
  
    
    
    var reachability = try? Reachability()
    var requestServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
    

//        self.edgesForExtendedLayout = []
//        navigationController?.navigationBar.isTranslucent = false
//        self.edgesForExtendedLayout = UIRectEdge.
        
        
        if reachability!.isReachable{
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    self.loadActivityIndicator()
                    let params = ["orderId":"\(UserDefaults.standard.value(forKey: "orderId"))"]
                    print(params)
                    
                    self.requestServer.connectToServer(myUrl: viewDetailsUrl!, params: params as AnyObject)
                }
                self.loadActivityIndicator()
            }
                 
               }
               else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
    }
    
    func loadActivityIndicator()
    {
if LocalizationSystem.sharedInstance.getLanguage() == "en"{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "Processing";
activityLoader?.detailsLabel.text = "Please Wait";
activityLoader?.isUserInteractionEnabled = false;
}
else{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "معالجة";
activityLoader?.detailsLabel.text = "فضلا النتظار";
activityLoader?.isUserInteractionEnabled = false;
}
       
    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
        if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {

           

                      
       }
   }

}
