//
//  ViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Pooya on 2018-03-12.
//  Copyright © 2018 Pooya Hatami. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices
import Alamofire
import Photos
import AVFoundation
import MBProgressHUD



class ViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
   
   
    
    @IBOutlet weak var counterLbl: UILabel!
    @IBOutlet weak var yourLbl: UILabel!
    @IBOutlet weak var linkForReferenceDotHeight: NSLayoutConstraint!
    @IBOutlet weak var linkForReferenceHeight: NSLayoutConstraint!
    @IBOutlet weak var linkForReferenceHeadingTop: NSLayoutConstraint!
    @IBOutlet weak var linkForReferenceHeadingHeight: NSLayoutConstraint!
    @IBOutlet weak var linkForReference: UILabel!
    @IBOutlet weak var linkForReferenceHeadingLbl: UILabel!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var uploadLbl: UILabel!
    @IBOutlet weak var redoLbl: UILabel!
    @IBOutlet weak var recordYourVideo: UILabel!
    @IBOutlet weak var makeSureYourInternet: UILabel!
    @IBOutlet weak var makeSuretoGive: UILabel!
    @IBOutlet weak var makeSureYourCameraLbl: UILabel!
    @IBOutlet weak var weRecommendLbl: UILabel!
    @IBOutlet weak var tipsLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var iamHappyHeadingLbl: UILabel!
    @IBOutlet weak var halagramDetailsHeadingLbl: UILabel!
    @IBOutlet weak var halagramOccasionHeadingLbl: UILabel!
    @IBOutlet weak var serviceTypeHeadingLbl: UILabel!
    @IBOutlet weak var dateOfRequestHeadingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var halagramOccasionHeight: NSLayoutConstraint!
    @IBOutlet weak var halagramOccasionDotHeight: NSLayoutConstraint!
    @IBOutlet weak var halagramOccasionHeadingTop: NSLayoutConstraint!
    @IBOutlet weak var halagramOccasionHeadingHeight: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var fromHeadingTopHeight: NSLayoutConstraint!
    @IBOutlet weak var toHeadingTopHeight: NSLayoutConstraint!
    @IBOutlet weak var fromHeadingLbl: UILabel!
    @IBOutlet weak var toHeadingLbl: UILabel!
    @IBOutlet weak var fromDotHeight: NSLayoutConstraint!
    @IBOutlet weak var toDotHeight: NSLayoutConstraint!
    @IBOutlet weak var toLblHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var fromLblHeight: NSLayoutConstraint!
    @IBOutlet weak var fromHeadingLblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var toHeadingLblHeight: NSLayoutConstraint!
    @IBOutlet weak var halagramOccassionLbl: UILabel!
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var toLbl: UILabel!
    @IBOutlet weak var stopView: UIView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var iamHappy: UILabel!
    @IBOutlet weak var halagramDetails: UILabel!
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var dateOfRequest: UILabel!
    @IBOutlet weak var recordVideoImage: UIImageView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var RecordButton: UIButton!
    var videoAndImageReview = UIImagePickerController()
    var videoURL: URL?
    var save = true
    var avPlayer = AVPlayer()
    var uploadBool = false
    var timeMin = 0
    var timeSec = 0
    weak var timer: Timer?
    var counter = 5
    var lbltimer:Timer?
    var session: AVCaptureSession?
    var userreponsevideoData = NSData()
    var userreponsethumbimageData = NSData()
    var reachability = try? Reachability()
    var requestServer = RequestToServer()
    var upload = true
    
    
    func connectToServer(myUrl : URL, params : AnyObject) {
        
        let header = [
            "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)",
            "lang":"\(LocalizationSystem.sharedInstance.getLanguage())"
               ]
        
        let request = NSMutableURLRequest(url:myUrl as URL)
        request.allHTTPHeaderFields = header
        request.httpMethod = "POST";
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                   print(json)
//                    if json != nil{
//
//                    }
                    if json.count > 0 {
//
                        if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                           
                           
                       let temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
                           
//                           print("\((temporaryArray.value(forKey: "requestDate") as! String))")
//                            print("\((temporaryArray.value(forKey: "shareHalagram") as! String))")
//                            if
                            
                            if("\((temporaryArray.value(forKey: "referenceLink") as! String))") != ""{
                                DispatchQueue.main.async {
                                    UserDefaults.standard.setValue("\((temporaryArray.value(forKey: "referenceLink") as! String))", forKey: "referenceLink")
                                    print("\((temporaryArray.value(forKey: "referenceLink") as! String))")
                                    self.viewHeight.constant = 280
                                    self.linkForReferenceHeight.constant = 18
                                    self.linkForReferenceHeadingTop.constant = 15
                                    self.linkForReferenceHeadingHeight.constant = 18
                                    self.linkForReferenceDotHeight.constant = 21
                                }
                            }
                            
                            if("\((temporaryArray.value(forKey: "serviceType") as! String))") == "Shout Out"{
                                
                                
                                DispatchQueue.main.async {
                                    
                                    self.halagramOccasionHeight.constant = 18
                                    self.halagramOccasionDotHeight.constant = 21
                                    self.halagramOccasionHeadingHeight.constant = 18
                                    self.halagramOccasionHeadingTop.constant = 15
                                    self.toHeadingLblHeight.constant = 18
                                    self.toLblHeight.constant = 18
                                    self.fromHeadingLblHeight.constant = 18
                                    self.fromLblHeight.constant = 18
                                    self.toDotHeight.constant = 21
                                    self.fromDotHeight.constant = 21
                                    self.toHeadingTopHeight.constant = 15
                                    self.fromHeadingTopHeight.constant = 15
                                    self.viewHeight.constant = 380
                                }
                            }
                            
                            
                            
                            DispatchQueue.main.async {
                                self.dateOfRequest.text = "\((temporaryArray.value(forKey: "requestDate") as! String))"
                                self.serviceType.text = "\((temporaryArray.value(forKey: "serviceType") as! String))"
                                self.halagramDetails.text = "\((temporaryArray.value(forKey: "details") as! String))"
                                self.iamHappy.text = "\((temporaryArray.value(forKey: "shareHalagram") as! String))"
                                self.toLbl.text = "\((temporaryArray.value(forKey: "requestTo") as! String))"
                                self.fromLbl.text = "\((temporaryArray.value(forKey: "requestFrom") as! String))"
                                self.halagramOccassionLbl.text = "\((temporaryArray.value(forKey: "occasion") as! String))"
                                self.linkForReference.text = "\((temporaryArray.value(forKey: "referenceLink") as! String))"
                                
                                
                                activityLoader?.hide(animated: true)
                            }
                            
                    }

                }
            }
            }catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    func connectToServerTesting(myUrl : URL, params : AnyObject) {
        
        let header = [
            "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)",
            "lang":"\(LocalizationSystem.sharedInstance.getLanguage())"
               ]
        
        let request = NSMutableURLRequest(url:myUrl as URL)
        request.allHTTPHeaderFields = header
        request.httpMethod = "POST";
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        //create the session object
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                   print(json)
                    
                    if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                        

                        print("uploaded")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        DispatchQueue.main.async {
                            
                            var totalCount = UserDefaults.standard.value(forKey: "totalCount") as! Int
                            totalCount = totalCount-1;
                            UserDefaults.standard.setValue(totalCount, forKey: "totalCount")
                            
                            
                            let vc = storyboard.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController
                            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                                let alert = UIAlertController(title: "",message: "Your Halagram Video successfully uploded",preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Ok",style: UIAlertActionStyle.default,
                                                              handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                let alert = UIAlertController(title: "",message: "تم تحميل هلاجرامك بنجاح",preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "موافق",style: UIAlertActionStyle.default,
                                                              handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
                                self.present(alert, animated: true, completion: nil)
                            }

                            self.uploadBool = false
                        }
                        DispatchQueue.main.async {
                            activityLoader?.hide(animated: true)
                            self.view.isUserInteractionEnabled = true

                        }
                       
                    }
                    else{
                        print("not uploaded")
                        DispatchQueue.main.async {
                            activityLoader?.hide(animated: true)
                            self.view.isUserInteractionEnabled = true

                        }
                    }
                    
                    

            }
            }catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }

    override func viewDidDisappear(_ animated: Bool) {
        resetTimerToZero()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
//        createSessionFirst()
        
        totalLbl.makeCircular()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{

            linkForReference.font.fontChanges(font: "AvenirArabic-Medium", lable: linkForReference)
            linkForReferenceHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: linkForReferenceHeadingLbl)
            
            dateOfRequestHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: dateOfRequestHeadingLbl)
            serviceTypeHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: serviceTypeHeadingLbl)
            dateOfRequest.font.fontChange(font: "AvenirArabic-Medium", lable: dateOfRequest)
            serviceType.font.fontChange(font: "AvenirArabic-Medium", lable: serviceType)
            toHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: toHeadingLbl)
            fromHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: fromHeadingLbl)
            fromLbl.font.fontChange(font: "AvenirArabic-Medium", lable: fromLbl)
            halagramOccasionHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: halagramOccasionHeadingLbl)
            halagramDetailsHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: halagramDetailsHeadingLbl)
            halagramDetails.font.fontChange(font: "AvenirArabic-Medium", lable: halagramDetails)
            iamHappyHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: iamHappyHeadingLbl)
            iamHappy.font.fontChange(font: "AvenirArabic-Medium", lable: iamHappy)
            tipsLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: tipsLbl)
            weRecommendLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: weRecommendLbl)
            makeSureYourCameraLbl.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSureYourCameraLbl)
            makeSuretoGive.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSuretoGive)
            makeSureYourInternet.font.fontChanges(font: "AvenirArabic-Medium", lable: makeSureYourInternet)
            recordYourVideo.font.fontChangesSize(font: "AvenirArabic-Medium", lable: recordYourVideo, size: 20)
            toLbl.font.fontChange(font: "AvenirArabic-Medium", lable: toLbl)
            halagramOccassionLbl.font.fontChange(font: "AvenirArabic-Medium", lable: halagramOccassionLbl)
            
            
            headerLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: headerLbl)
            noteLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: noteLbl)
            redoLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: redoLbl)
            uploadLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: uploadLbl)
            

        }
        else{
            toLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: toLbl)
            halagramOccassionLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccassionLbl)
            linkForReference.font.fontChanges(font: "DiodrumRounded-Medium", lable: linkForReference)
            linkForReferenceHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: linkForReferenceHeadingLbl)
            dateOfRequestHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequestHeadingLbl)
            serviceTypeHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: serviceTypeHeadingLbl)
            dateOfRequest.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequest)
            serviceType.font.fontChange(font: "DiodrumRounded-Medium", lable: serviceType)
            toHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: toHeadingLbl)
            fromHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: fromHeadingLbl)
            fromLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: fromLbl)
            halagramOccasionHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccasionHeadingLbl)
            halagramDetailsHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramDetailsHeadingLbl)
            halagramDetails.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramDetails)
            iamHappyHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: iamHappyHeadingLbl)
            iamHappy.font.fontChange(font: "DiodrumRounded-Medium", lable: iamHappy)
            tipsLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: tipsLbl)
            weRecommendLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: weRecommendLbl)
            makeSureYourCameraLbl.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSureYourCameraLbl)
            makeSuretoGive.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSuretoGive)
            makeSureYourInternet.font.fontChanges(font: "DiodrumRounded-Medium", lable: makeSureYourInternet)
            recordYourVideo.font.fontChangesSize(font: "DiodrumRounded-Medium", lable: recordYourVideo, size: 20)
            
            
            
            headerLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: headerLbl)
            noteLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: noteLbl)
            redoLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: redoLbl)
            uploadLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: uploadLbl)

            
            

//            requestedByHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: requestedByHeadingLbl)
//            halagramOccasionHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccasionHeadingLbl)
//            dateOfRequestHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequestHeadingLbl)
//            requestedByLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: requestedByLbl)
//            halagramOccasionLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: halagramOccasionLbl)
//            dateOfRequestLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: dateOfRequestLbl)
//            viewDetailsLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: viewDetailsLbl)
//            daysRemainingLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: daysRemainingLbl)

        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        DispatchQueue.main.async {
            self.englishBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "English", comment: ""), for: .normal)
            
            self.headerLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New Requests", comment: "")
            self.dateOfRequestHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Date of Request", comment: "")
            self.serviceTypeHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Service Type", comment: "")
            self.toHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "To", comment: "")
            self.fromHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "From", comment: "")
            self.halagramOccasionHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Halagram Occassion", comment: "")
            self.halagramDetailsHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Halagram Details", comment: "")
            self.iamHappyHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "I am Happy for Halagram to Share my Video", comment: "")
            self.noteLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Note:", comment: "")
            self.tipsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Tips to make sure you shoot the best video.", comment: "")
            self.weRecommendLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "1. We recommend that your device is in potrait mode while taking the video.", comment: "")
            self.makeSureYourCameraLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "2. Make sure your camera isn't being used by another application while you are taking the video.", comment: "")
            self.makeSuretoGive.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "3. Make sure to give permission to the application or web browser to access the camera.", comment: "")
            self.makeSureYourInternet.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "4. Make sure your internet connection is stable while uploading and that you double check your video before sharing.", comment: "")
            self.recordYourVideo.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Record Your Video", comment: "")
            
            self.redoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Redo", comment: "")
            self.uploadLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Upload", comment: "")
            self.linkForReferenceHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Link for Reference", comment: "")
            self.createSessionFirst()
           
        }
//        DispatchQueue.main.async {
           
//        }
    }
    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self)
        avPlayer.pause()
        session?.stopRunning()
    }

    fileprivate func startTimer(){
        
        // if you want the timer to reset to 0 every time the user presses record you can uncomment out either of these 2 lines

        // timeSec = 0
        // timeMin = 0

        // If you don't use the 2 lines above then the timer will continue from whatever time it was stopped at
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        yourLbl.text = timeNow

        stopTimer() // stop it at it's current time before starting it again
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
                    self?.timerTick()
                }
    }
    @objc fileprivate func timerTick(){
         timeSec += 1
            
         if timeSec == 60{
             timeSec = 0
             timeMin += 1
         }
            
         let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
            
         yourLbl.text = timeNow
    }

    // resets both vars back to 0 and when the timer starts again it will start at 0
    @objc fileprivate func resetTimerToZero(){
         timeSec = 0
         timeMin = 0
         stopTimer()
    }

    // if you need to reset the timer to 0 and yourLabel.txt back to 00:00
    @objc fileprivate func resetTimerAndLabel(){

         resetTimerToZero()
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
    }

    // stops the timer at it's current time
    @objc fileprivate func stopTimer(){

         timer?.invalidate()
    }

    @objc func reachabilityChanged(note:Notification) -> Bool {
          let reachability = note.object as! Reachability
          
          if reachability.isReachable{
              print("reachable")
            if uploadBool == true{
                self.loadActivityIndicator(text: "Loading")
                let params = ["orderId":UserDefaults.standard.value(forKey: "orderId")!]
//                requestServer.connectToServer(myUrl: viewDetailsTesting!, params: params as AnyObject)
                self.connectToServerTesting(myUrl: viewDetailsTesting!, params: params as AnyObject)
                self.view.isUserInteractionEnabled = false

            }
            return true
          }
          else{
              print("unreachable")
//            alert(message: "No internet connection", title: "")
           return false
          }
          
      }
    
    
            override func viewDidLoad() {
                super.viewDidLoad()
                
                self.counterLbl.isHidden = true
                
              
                
//                requestServer.delegate = self
                
                NotificationCenter.default.addObserver(
                                      self,
                                      selector: #selector(reachabilityChanged),
                                      name: Notification.Name.reachabilityChanged,
                                      object: reachability
                       )
                       do{
                           try reachability?.startNotifier()
                       }catch{
                           print("could not respond")
                       }
                
                
                UserDefaults.standard.set(true, forKey: "slidemenu")
                
                halagramOccasionHeight.constant = 0
                halagramOccasionDotHeight.constant = 0
                halagramOccasionHeadingHeight.constant = 0
                halagramOccasionHeadingTop.constant = 0
                toHeadingLblHeight.constant = 0
                toLblHeight.constant = 0
                fromHeadingLblHeight.constant = 0
                fromLblHeight.constant = 0
                toDotHeight.constant = 0
                fromDotHeight.constant = 0
                toHeadingTopHeight.constant = 0
                fromHeadingTopHeight.constant = 0
                viewHeight.constant = 250
                linkForReferenceHeight.constant = 0
                linkForReferenceHeadingTop.constant = 0
                linkForReferenceHeadingHeight.constant = 0
                linkForReferenceDotHeight.constant = 0
                
//                toHeadingLbl.removeFromSuperview()
//                fromHeadingLbl.removeFromSuperview()
//                toLbl.removeFromSuperview()
//                fromLbl.removeFromSuperview()
                
                
                
                
                
                
                playBtn.isEnabled = false
                
                self.startView.cornerRadius(view: self.startView)
                self.stopView.cornerRadius(view: self.stopView)
                
                self.stopView.isHidden = true
                self.startView.isHidden = true

                self.uploadBtn.isHidden = true
                self.videoView.isHidden = true
                self.retakeBtn.isHidden = true
                 
                if reachability!.isReachable{
                    DispatchQueue.main.async {
                        DispatchQueue.main.async {
                            self.loadActivityIndicator(text: "Loading")
                            print(UserDefaults.standard.value(forKey: "orderId")!)
                            let params = ["orderId":UserDefaults.standard.value(forKey: "orderId")!]
                            print(params)
                            
                            self.connectToServer(myUrl: viewDetailsUrl!, params: params as AnyObject)
                        }
                        
                    }
                         
                       }
                       else{
                        if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                            self.alert(message: "No internet connection", title: "")
                        }
                        else{
                            self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                        }
                                  
                              }
                
                
            }
    func loadActivityIndicator(text:String)
                   {
                       activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
//                       activityLoader?.label.text = "Loading";
                       activityLoader?.detailsLabel.text = text;
                       activityLoader?.isUserInteractionEnabled = false;
                   }
    
    override func viewDidAppear(_ animated: Bool) {
                super.viewDidAppear(animated)
            }
    
    func setUpAllViews()  {
        cameraView.cornerRadius(view: cameraView)
    }
    func createSessionFirst() {
        
        DispatchQueue.main.async { [self] in
            
        
        let audioDevice = AVCaptureDevice.default(for: .audio)
        var audioInput: AVCaptureDeviceInput? = nil

          var input: AVCaptureDeviceInput?
        
//        var audioInput = AVCaptureDevice.default(for: AVMediaType.audio)
        
          let  movieFileOutput = AVCaptureMovieFileOutput()
          var prevLayer: AVCaptureVideoPreviewLayer?
          prevLayer?.frame.size = cameraView.frame.size
          session = AVCaptureSession()
          let error: NSError? = nil
        do { input = try AVCaptureDeviceInput(device: self.cameraWithPosition(position: .front)!)
            if let audioDevice = audioDevice {
                    audioInput = try AVCaptureDeviceInput(device: audioDevice)
                }
            
//            audioInput = try AVCaptureDeviceInput(device: audioInput!)



        } catch {return}
          if error == nil {
            session?.addInput(input!)
            if let audioInput = audioInput {
                session?.addInput(audioInput)
               
            }
            
            
            
//            session?.addInput(audioInput!)
//            try self.session!.addInput(AVCaptureDeviceInput(device: audioInput!))

            
          } else {
              print("camera input error: \(error)")
          }
        prevLayer = AVCaptureVideoPreviewLayer(session: session!)
          prevLayer?.frame.size = cameraView.frame.size
        prevLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        prevLayer?.connection!.videoOrientation = .portrait
        cameraView.layer.addSublayer(prevLayer!)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//          let  filemainurl = NSURL(string: ("\(documentsURL.URLByAppendingPathComponent("temp")!)" + ".mov"))

        let  filemainurl = documentsURL.appendingPathComponent("temp.mov")
        

          let maxDuration: CMTime = CMTimeMake(600, 10)
          movieFileOutput.maxRecordedDuration = maxDuration
          movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024
          if self.session!.canAddOutput(movieFileOutput) {
              self.session!.addOutput(movieFileOutput)
            self.session?.sessionPreset =  .medium
          }
          session?.startRunning()
//        movieFileOutput.startRecording(to: filemainurl, recordingDelegate: self)

      }
    }
   
    
    func createSession() {

        startTimer()

        let audioDevice = AVCaptureDevice.default(for: .audio)
        var audioInput: AVCaptureDeviceInput? = nil

          var input: AVCaptureDeviceInput?
        
//        var audioInput = AVCaptureDevice.default(for: AVMediaType.audio)
        
          let  movieFileOutput = AVCaptureMovieFileOutput()
          var prevLayer: AVCaptureVideoPreviewLayer?
          prevLayer?.frame.size = cameraView.frame.size
          session = AVCaptureSession()
          let error: NSError? = nil
        do { input = try AVCaptureDeviceInput(device: self.cameraWithPosition(position: .front)!)
            if let audioDevice = audioDevice {
                    audioInput = try AVCaptureDeviceInput(device: audioDevice)
                }
            
//            audioInput = try AVCaptureDeviceInput(device: audioInput!)



        } catch {return}
          if error == nil {
            session?.addInput(input!)
            if let audioInput = audioInput {
                session?.addInput(audioInput)
               
            }
            
            
            
//            session?.addInput(audioInput!)
//            try self.session!.addInput(AVCaptureDeviceInput(device: audioInput!))

            
          } else {
              print("camera input error: \(error)")
          }
        prevLayer = AVCaptureVideoPreviewLayer(session: session!)
          prevLayer?.frame.size = cameraView.frame.size
        prevLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        prevLayer?.connection!.videoOrientation = .portrait
        cameraView.layer.addSublayer(prevLayer!)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//          let  filemainurl = NSURL(string: ("\(documentsURL.URLByAppendingPathComponent("temp")!)" + ".mov"))

        let  filemainurl = documentsURL.appendingPathComponent("temp.mov")
        

          let maxDuration: CMTime = CMTimeMake(600, 10)
          movieFileOutput.maxRecordedDuration = maxDuration
          movieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024
          if self.session!.canAddOutput(movieFileOutput) {
              self.session!.addOutput(movieFileOutput)
            self.session?.sessionPreset =  .medium
          }
          session?.startRunning()
        movieFileOutput.startRecording(to: filemainurl, recordingDelegate: self)

      }
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
          for device in devices {
              if device.position == position {
                  return device as? AVCaptureDevice
              }
          }
          return nil
      }
    
    
    @IBAction func playVideo(_ sender: Any) {
        
        cameraView.isHidden = true
        videoView.isHidden = false
        
//        let avPlayer = AVPlayer()
        var avPlayerLayer: AVPlayerLayer!
        
        
        var path  = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        var folder: String = path[0] as! String
           NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
        let wordToRemove = "Preferences"


        if let range = folder.range(of: wordToRemove) {
            folder.removeSubrange(range)
        }
       folder += "/temp.mov"
        
        var str = "file//"

        let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
        folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
        
        print(folder)
        
        
        var videoURL: URL!
        videoURL = URL(string: folder)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
                avPlayerLayer.frame = videoView.bounds
                avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                 videoView.layer.sublayers = nil
                videoView.layer.insertSublayer(avPlayerLayer, at: 0)
            
                view.layoutIfNeeded()
            
                let playerItem = AVPlayerItem(url: videoURL as URL)
                avPlayer.replaceCurrentItem(with: playerItem)
            
                avPlayer.play()
//        playBtn.isEnabled = false
        
    }
    
    
    

    
    @IBAction func upload(_ sender: Any) {
        
        uploadBool = true

        
        DispatchQueue.main.async {
            
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                self.loadActivityIndicator(text: "Uploading will take few minutes depending on the size of the video and the internet speed")
            }
            else{
                self.loadActivityIndicator(text: "سيستغرق التحميل بضع دقائق حسب حجم الفيديو وسرعة الإنترنت")
            }
           
            self.view.isUserInteractionEnabled = false

        }
        
        
        
        
        var path: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [AnyObject]
        var folder: String = path[0] as! String
           NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
        let wordToRemove = "Preferences"


        if let range = folder.range(of: wordToRemove) {
            folder.removeSubrange(range)
        }
       folder += "/temp.mov"
        
        var str = "file//"

        let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
        folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
        
        print(folder)
        
       let urlString = URL(string: folder)
            print(urlString)
       
        
        
        
        encodeVideo(at: urlString!) { (url,Error) in
            print(url )
            
            if let orderId = Int(UserDefaults.standard.value(forKey: "orderId") as! String) {
                
                let autoken = Int(UserDefaults.standard.value(forKey: "orderId") as! String)
                print("\(UserDefaults.standard.string(forKey: "token")!)")
                let parameters = [
                    "orderId": "\(orderId)"
                 ]
                  print(parameters)

                print("\(String(describing: UserDefaults.standard.value(forKey: "token")))")
                 let headers: HTTPHeaders = [
                          "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                          "cache-control": "no-cache",
                    "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)"
                        ]
                        do {
                //                                        let data = try Data(contentsOf: videoUrl, options: .mappedIfSafe)
                //                                        print(data)
                            AF.upload(
                                           multipartFormData: { multipartFormData in
                                            for (key, value) in parameters {
                                                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                                            }
                                             multipartFormData.append(url!, withName: "halagram")

                                           print(parameters)


                                       },
                                           to: "https://halagram.me/backend/app/request/upload", method: .post , headers: headers)
                                .response { resp in switch resp.result{
                               
                                case .success(_):
                                    print("success")

                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                                    var totalCount = UserDefaults.standard.value(forKey: "totalCount") as! Int
                                    totalCount = totalCount-1;
                                    UserDefaults.standard.setValue(totalCount, forKey: "totalCount")
                                    
                                         let vc = storyboard.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController
                                                                   
                                       
                                    if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                                        let alert = UIAlertController(title: "",message: "Your Halagram Video successfully uploded",preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Ok",style: UIAlertActionStyle.default,
                                                                      handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else{
                                        let alert = UIAlertController(title: "",message: "تم تحميل هلاجرامك بنجاح",preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "موافق",style: UIAlertActionStyle.default,
                                                                      handler: {(alert: UIAlertAction!) in self.navigationController?.pushViewController(vc, animated: true)}))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                   
                                   
                                   
                                    
                                    activityLoader?.hide(animated: true)
                                    self.view.isUserInteractionEnabled = true
                                    
//                                    print(resp.result)
//                                    print(resp.error)
//                                    print(resp.data)
//                                    print(resp.description)
                                case .failure(_):
                                    print("fail")
                                    if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                                        self.alert(message: "Upload Failed - Please Check Internet Connection and Click on “Upload HalaGram” Again")
                                    }
                                    else{
                                        self.alert(message: "لم يتم تحميل الفيديو، فضلا التأكد من الاتصال بالانترنت و انقر تحميل الهلاجرام")
                                    }
                                    activityLoader?.hide(animated: true)
                                    self.view.isUserInteractionEnabled = true
                                }
                                               print(resp)


                                       }

                                                    }
                        catch  {
                                                    }
            }




        }

    }
    @IBAction func stop(_ sender: Any) {
       
    }
    
    @IBAction func menuBtnTapped(_ sender: Any) {
        if (UserDefaults.standard.bool(forKey: "slidemenu")){
            UserDefaults.standard.set(false, forKey: "slidemenu")
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
//                UserDefaults.standard.set(false, forKey: "slidemenu")
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
               
            }
            else{
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 60, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
            }
           
           
            
            
            

           
        }
       
       
        
        
    }
    
    
    
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    @objc func update() {
        if(counter > 0) {
            avPlayer.pause()
            counterLbl.isHidden = false
            let ncounter = counter-1
                counter = ncounter
            counterLbl.text = "Recording will start in \n\(ncounter)"
        }
        else{
            RecordButton.isHidden = false
            lbltimer?.invalidate()
            counterLbl.isHidden = true
            avPlayer.pause()
            createSession()
            
            DispatchQueue.main.async {
            self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
            self.save = false
                        }
        }
    }
    
    @IBAction func RecordAction(_ sender: UIButton) {
        
        session?.stopRunning()
        
        if save == true{
            self.lbltimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(update), userInfo: nil, repeats: true)
            RecordButton.isHidden = true
//            createSession()
//            
//            DispatchQueue.main.async {
//                self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
//                self.save = false
//            }
           
        }
        else{
            stopTimer()
            self.session?.stopRunning()
            self.recordVideoImage.isHidden = true
            self.uploadBtn.isHidden = false
            self.retakeBtn.isHidden = false
            self.startView.isHidden = false
            self.stopView.isHidden = false
            playBtn.isEnabled = true
            
            cameraView.isHidden = true
            videoView.isHidden = false
            
//            let avPlayer = AVPlayer()
            var avPlayerLayer: AVPlayerLayer!
            
            
            var path: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [AnyObject]
            var folder: String = path[0] as! String
               NSLog("Your NSUserDefaults are stored in this folder: %@/Preferences", folder)
            let wordToRemove = "Preferences"


            if let range = folder.range(of: wordToRemove) {
                folder.removeSubrange(range)
            }
           folder += "/temp.mov"
            
            var str = "file//"

            let index = str.characters.index(str.characters.startIndex, offsetBy: 0)  //here you define a place (index) to insert at
            folder.characters.insert(contentsOf: "file://", at: index)  //and here you insert
            
            print(folder)
            
            
            var videoURL: URL!
            videoURL = URL(string: folder)
            avPlayerLayer = AVPlayerLayer(player: avPlayer)
                    avPlayerLayer.frame = videoView.bounds
                    avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    videoView.layer.sublayers = nil
                    videoView.layer.insertSublayer(avPlayerLayer, at: 0)
                
            videoView.layoutIfNeeded()
                
                    let playerItem = AVPlayerItem(url: videoURL as URL)
                    avPlayer.replaceCurrentItem(with: playerItem)
                
                    avPlayer.play()
            
        }
       
        
        
        


    }
    
    @IBAction func retake(_ sender: Any) {
        resetTimerToZero()
        yourLbl.text = String(format: "%02d:%02d", timeMin, timeSec)
        
        if reachability!.isReachable{
            avPlayer.pause()
            DispatchQueue.main.async {
                self.recordVideoImage.isHidden = false
                self.recordVideoImage.image = UIImage(named: "icons8-stop-64")
                
            }
            cameraView.isHidden = false
            videoView.isHidden = true
            retakeBtn.isHidden = true
            uploadBtn.isHidden = true
            stopView.isHidden = true
            startView.isHidden = true
            createSession()
                 
               }
               else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
        
       
        
    }
    
    
    var  playerController :  AVPlayerViewController!
    
    @objc func didfinishPlaying(note : NSNotification){
        playerController.dismiss(animated: true, completion: nil)

        let alertView = UIAlertController(title: "Finished", message: "Video finished", preferredStyle: .alert)

        alertView.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alertView, animated: true, completion: nil)

       }
    
    @IBAction func play(_ sender: Any) {
//        let path = Bundle.main.path(forResource: "Video", ofType: "mov")
//               let url = NSURL(fileURLWithPath:  path!)
//        let player = AVPlayer(url: url as URL)
//               playerController = AVPlayerViewController()
//
//        NotificationCenter.default.addObserver(self, selector: #selector(didfinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
//
//               playerController.player = player
//               playerController.player?.play()
        
        
        
        
       //        imagePicker.sourceType = .Camera
       //        imagePicker.cameraDevice = .Front
       //        imagePicker.sourceType = .Camera
       //        imagePicker.mediaTypes = [kUTTypeMovie as String]
       //        imagePicker.allowsEditing = false
       //        imagePicker.delegate = self
       //          presentViewController(imagePicker, animated: false, completion: {})

        self.present(playerController, animated: true, completion: nil)

    }
   
   
    
    // Don't forget to import AVKit

    func encodeVideo(at videoURL: URL, completionHandler: ((URL?, Error?) -> Void)?)  {
        let avAsset = AVURLAsset(url: videoURL, options: nil)

        let startDate = Date()

        //Create Export session
        guard let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough) else {
            completionHandler?(nil, nil)
            return
        }

        //Creating temp path to save the converted video
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
        let filePath = documentsDirectory.appendingPathComponent("rendered-Video.mp4")

        //Check if the file already exists then remove the previous file
        if FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.removeItem(at: filePath)
            } catch {
                completionHandler?(nil, error)
            }
        }

        exportSession.outputURL = filePath
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, avAsset.duration)
        exportSession.timeRange = range

        exportSession.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession.status {
            case .failed:
                print(exportSession.error ?? "NO ERROR")
                completionHandler?(nil, exportSession.error)
            case .cancelled:
                print("Export canceled")
                completionHandler?(nil, nil)
            case .completed:
                //Video conversion finished
                let endDate = Date()

                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful!")
                print(exportSession.outputURL ?? "NO OUTPUT URL")
                completionHandler?(exportSession.outputURL, nil)

                default: break
            }

        })
    }
    
    @IBAction func englishBtnTapped(_ sender: Any) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
            
        }
        else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")

//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
//            }

           


            UserDefaults.standard.setValue("arabic", forKey: "languageSetting")
            
        }
        
        
        let window = self.view.superview
        self.view.removeFromSuperview()
        window?.addSubview(self.view)
        
        viewWillAppear(true)
    }
    
    
    
    @IBAction func reference(_ sender: Any) {
        let urlString = UserDefaults.standard.value(forKey: "referenceLink")
        guard let url = URL(string: urlString as! String) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    

}
extension ViewController: AVCaptureFileOutputRecordingDelegate{
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print(outputFileURL)
        
//        UserDefaults.standard.setValue("\(outputFileURL)", forKey: "galleryUrl")
        UserDefaults.standard.set(outputFileURL, forKey: "galleryUrl")

        
        
        if upload == true{
//            saveVideoToAlbum(outputFileURL, nil)
        }
        
       
        
        

        
        else{
            encodeVideo(at: outputFileURL) { (url,Error) in
                print(url )

                let parameters = [
                    "orderId": "\(String(describing: UserDefaults.standard.value(forKey: "orderId")))"
                 ]
                  print(parameters)

                 let headers: HTTPHeaders = [
                          "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                          "cache-control": "no-cache",
                          "postman-token": "a890d1d0-de05-2e4b-1f9c-87828e06c0c6"
                        ]
                        do {
                //                                        let data = try Data(contentsOf: videoUrl, options: .mappedIfSafe)
                //                                        print(data)
                            AF.upload(
                                           multipartFormData: { multipartFormData in
                                            for (key, value) in parameters {
                                                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                                            }
                                             multipartFormData.append(url!, withName: "halagram")
                                          
                                           print(parameters)
                                            
                                            
                                       },
                                           to: "https://halagram.me/backend/api/video/upload", method: .post , headers: headers)
                                           .response { resp in
                                               print(resp)


                                       }

                                                    }
                        catch  {
                                                    }


            }

        }
        

        

    }
    func requestAuthorization(completion: @escaping ()->Void) {
            if PHPhotoLibrary.authorizationStatus() == .notDetermined {
                PHPhotoLibrary.requestAuthorization { (status) in
                    DispatchQueue.main.async {
                        completion()
                    }
                }
            } else if PHPhotoLibrary.authorizationStatus() == .authorized{
                completion()
            }
        }
//    func saveVideoToAlbum(_ outputURL: URL, _ completion: ((Error?) -> Void)?) {
//            requestAuthorization {
//                PHPhotoLibrary.shared().performChanges({
//                    let request = PHAssetCreationRequest.forAsset()
//                    request.addResource(with: .video, fileURL: outputURL, options: nil)
//                }) { (result, error) in
//                    DispatchQueue.main.async {
//                        if let error = error {
//                            print(error.localizedDescription)
//                        } else {
//                            print("Saved successfully")
//
//
//                        }
//                        completion?(error)
//                    }
//                }
//            }
//        }

    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        print(fileURL)
    }



}
