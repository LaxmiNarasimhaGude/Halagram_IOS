//
//  DashBoardViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 23/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD
class DashBoardViewController: UIViewController,dataReceivedFromServerDelegate {
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
         if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
            
           
            
                       let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
            
                        print(temporaryArray)

            

            
            
            DispatchQueue.main.async {
                self.totalVisitorsLbl.text = temporaryArray.value(forKey: "totalVisitors") as? String
//                print(temporaryArray.value(forKey: "totalRequest"))
                
                
                self.totalRequestsLbl.text = "\(temporaryArray.value(forKey: "totalRequest")!)"
                self.totalEarningsLbl.text = "\(temporaryArray.value(forKey: "totalEarning")!)"
                self.convertionRateLbl.text = "\(temporaryArray.value(forKey: "conversionRate")!)%"
                let copmpletionRate = Double(round(1000*Double("\(temporaryArray.value(forKey: "completionRate")!)")!)/1000)
                
                self.completionRateLbl.text = "\(String(copmpletionRate))%"
                
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.newRequestLbl.text = "You have \(temporaryArray.value(forKey: "newRequest")!) New Requests"
                }
                else{
                    self.newRequestLbl.text = "لديك \(temporaryArray.value(forKey: "newRequest")!) طلبات جديدة"
                }
               
                
                self.activityLoader?.hide(animated: true)
            }
            
            
            //setObject
                       
        }
         else if(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "token expired")||(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "invalid token"){
            
            DispatchQueue.main.async {
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

                        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                                let navigationController = UINavigationController(rootViewController: loginVC)



                        appDel.window?.rootViewController = navigationController
                           }

            
         }
    }
    
    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    @IBOutlet weak var viewDetailsLbl: UILabel!
    @IBOutlet weak var converstionRateHeadingLbl: UILabel!
    @IBOutlet weak var completionRateHeadingLbl: UILabel!
    @IBOutlet weak var totalRequestHeadingLbl: UILabel!
    @IBOutlet weak var totalEarningsHeadingLbl: UILabel!
    @IBOutlet weak var totalVisitorsHeadingLbl: UILabel!
    @IBOutlet weak var youHave1RequestLbl: UILabel!
    @IBOutlet weak var halagramYourFansLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var convertionRateLbl: UILabel!
    @IBOutlet weak var totalEarningsLbl: UILabel!
    @IBOutlet weak var totalVisitorsLbl: UILabel!
    @IBOutlet weak var totalRequestsLbl: UILabel!
    @IBOutlet weak var completionRateLbl: UILabel!
    @IBOutlet weak var newRequestLbl: UILabel!
    @IBOutlet weak var dashBoardLbl: UILabel!
    
   
    @IBOutlet weak var englishBtn: UIButton!
    
    @IBOutlet weak var viewDetailsVIew: UIView!
    @IBOutlet weak var completionView: UIView!
    @IBOutlet weak var conversionView: UIView!
    @IBOutlet weak var earningsView: UIView!
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var visitorsView: UIView!
    
    var reachability = try? Reachability()
     var requestServer = RequestToServer()
    var activityLoader : MBProgressHUD? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
//        UIApplication.shared.statusBarView?.backgroundColor = .red

        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            halagramYourFansLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: halagramYourFansLbl, size: 20.0)
            dashBoardLbl.font.fontChangesSize(font: "AvenirArabic-Medium", lable: dashBoardLbl, size: 20.0)
            englishBtn.titleLabel?.font.fontChange(font: "AvenirArabic-Medium", lable:englishBtn.titleLabel! )
            
//                        halagramYourFansLbl.font = UIFont(name:"AvenirArabic-Heavy", size:15)
            
            
           
            totalVisitorsHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: totalVisitorsHeadingLbl)
            totalRequestHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: totalRequestHeadingLbl)
            totalEarningsHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: totalEarningsHeadingLbl)
            converstionRateHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: converstionRateHeadingLbl)
            completionRateHeadingLbl.font.fontChange(font: "AvenirArabic-Medium", lable: completionRateHeadingLbl)
            viewDetailsLbl.font.fontChange(font: "AvenirArabic-Medium", lable: viewDetailsLbl)
            totalVisitorsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: totalVisitorsLbl, size: 20.0)
            totalRequestsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: totalRequestsLbl, size: 20.0)
            totalEarningsLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: totalEarningsLbl, size: 20.0)
            convertionRateLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: convertionRateLbl, size: 20.0)
            completionRateLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: completionRateLbl, size: 20.0)
            youHave1RequestLbl.font.fontChangesSize(font: "AvenirArabic-Heavy", lable: youHave1RequestLbl, size: 20.0)
            headerLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: headerLbl)

        }
        else{
            
            halagramYourFansLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: halagramYourFansLbl, size: 20.0)
            dashBoardLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: dashBoardLbl, size: 20.0)
            englishBtn.titleLabel?.font.fontChange(font: "AvenirArabic-Medium", lable:englishBtn.titleLabel! )
            totalVisitorsHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: totalVisitorsHeadingLbl)
            totalRequestHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: totalRequestHeadingLbl)
            totalEarningsHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: totalEarningsHeadingLbl)
            converstionRateHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: converstionRateHeadingLbl)
            completionRateHeadingLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: completionRateHeadingLbl)
            viewDetailsLbl.font.fontChange(font: "DiodrumRounded-Medium", lable: viewDetailsLbl)
            totalVisitorsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalVisitorsLbl, size: 20.0)
            totalRequestsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalRequestsLbl, size: 20.0)
            totalEarningsLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: totalEarningsLbl, size: 20.0)
            convertionRateLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: convertionRateLbl, size: 20.0)
            completionRateLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: completionRateLbl, size: 20.0)
            youHave1RequestLbl.font.fontChangesSize(font: "DiodrumRounded-Semibold", lable: youHave1RequestLbl, size: 20.0)
            headerLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: headerLbl)
//            loginToYourHalagramAccountLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
//            loginLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: loginToYourHalagramAccountLbl)
//            emailTxtField.font?.fontChange(font: "DiodrumRounded-Medium", lable: loginToYourHalagramAccountLbl)
//            passwordTxtField.font?.fontChange(font: "DiodrumRounded-Medium", lable: loginToYourHalagramAccountLbl)
        }
        
        
        
        
        self.getDataFromServer(myGetUrl: infoApi!)
        

           
        print(LocalizationSystem.sharedInstance.getLanguage())
        
        DispatchQueue.main.async {
            self.halagramYourFansLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Halagram Your Fans -", comment: "")
            self.youHave1RequestLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "You have 1 New Request", comment: "")
            self.totalVisitorsHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Total Visitors", comment: "")
            self.totalRequestHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Total Requests", comment: "")
            self.totalEarningsHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Total Earnings", comment: "")
            self.converstionRateHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Covertion Rate", comment: "")
            self.completionRateHeadingLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Completion Rate", comment: "")
            self.viewDetailsLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "View Details", comment: "")
            self.headerLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Dashboard", comment: "")
            self.englishBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "English", comment: ""), for: .normal)
            
        }
        
        
           
       }
    
    func updateAll()  {
        print((UserDefaults.standard.value(forKey: "isApproved")))
        
        let reviewCount:Int = (UserDefaults.standard.value(forKey: "reviewCount")) as! Int
        let requestCount:Int = (UserDefaults.standard.value(forKey: "requestCount")) as! Int
        let reactionCount:Int = (UserDefaults.standard.value(forKey: "reactionCount")) as! Int
        let totalCount:Int = reviewCount+requestCount+reactionCount
        UserDefaults.standard.setValue(totalCount, forKey: "totalCount")
        
        
        
        
//        print(((UserDefaults.standard.value(forKey: "reviewCount")) as! Int), +((UserDefaults.standard.value(forKey: "requestCount")) as! Int))
        
        if (((UserDefaults.standard.value(forKey: "isApproved")) as! Bool)==false){
            DispatchQueue.main.async {
                self.visitorsView.isHidden = true
                self.requestView.isHidden = true
                self.earningsView.isHidden = true
                self.conversionView.isHidden = true
                self.completionView.isHidden = true
                self.viewDetailsVIew.isHidden = true
                self.halagramYourFansLbl.isHidden = true
                self.youHave1RequestLbl.isHidden = true
                self.lineLbl.isHidden = true
                self.dashBoardLbl.text = "\((UserDefaults.standard.value(forKey: "dashboardMsg"))!)"
                
                self.view.isHidden = false
                
            }
           
//            alertMessage(message: "\((UserDefaults.standard.value(forKey: "dashboardMsg"))!)", title: "Ok")
            
//            self.requestView.isHidden = true
            
        }
        else{
            DispatchQueue.main.async {
                if totalCount>0{
                    self.totalLbl.isHidden = false
                    self.totalLbl.text = "\(totalCount)"
                }
                else{
                    self.totalLbl.isHidden = true
                }
                self.dashBoardLbl.isHidden = true
                self.view.isHidden = false
                self.dashBoardLbl.isHidden = true
                self.setAllView()
            }
          
            
            
            requestServer.delegate = self
           
           
           if reachability!.isReachable{
               
               
               
 
               
               self.requestServer.getDataFromServer(myGetUrl: counterUrl!)
               DispatchQueue.main.async {
                              self.loadActivityIndicator()
                          }
           }
           else{
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                self.alert(message: "No internet connection", title: "")
            }
            else{
                self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
            }
                      
                  }
        }

    }
    
    func getDataFromServer(myGetUrl : URL) {
         let url = myGetUrl
         
         //create the session object
         let session = URLSession.shared
         
         let header = [
             "authtoken":"\(UserDefaults.standard.string(forKey: "token")!)",
             "lang":"\(LocalizationSystem.sharedInstance.getLanguage())"
            
         ]
         
         
         //now create the URLRequest object using the url object
         var request = URLRequest(url: url )
         
         request.allHTTPHeaderFields = header
         
         //create dataTask using the session object to send data to the server
         let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
             
             guard error == nil else {
                 return
             }
             
             guard let data = data else {
                 return
             }
             
             do {
                 //create json object from data
                 if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                     print(json)
                     if json.count > 0 {

                        if ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
                                    let temporaryArray = ((json.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSDictionary)
                         
                            print(temporaryArray)

                                
                            DispatchQueue.main.async {
                                
                                UserDefaults.standard.setValue(temporaryArray.value(forKey: "dashboardMsg"), forKey: "dashboardMsg")
                                UserDefaults.standard.setValue(temporaryArray.value(forKey: "isApproved"), forKey: "isApproved")
                                UserDefaults.standard.setValue(temporaryArray.value(forKey: "reactionCount"), forKey: "reactionCount")
                                UserDefaults.standard.setValue(temporaryArray.value(forKey: "requestCount"), forKey: "requestCount")
                                UserDefaults.standard.setValue(temporaryArray.value(forKey: "reviewCount"), forKey: "reviewCount")
                                
                                

                                
                            }

                             DispatchQueue.main.async {
                                 self.activityLoader?.hide(animated: true)


                             }
                            
                             
                             

                                    
                         
                         }
                        DispatchQueue.main.async {
                            self.updateAll()
                        }
                       
                         
                         
                     }
                 
                   
                 }
             } catch let error {
                 print(error.localizedDescription)
             }
         })
         task.resume()
     }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "slidemenu")

    
        
        setAllView()
        
        
        
        
        if(UserDefaults.standard.value(forKey: "isUpdateAvailable") as! String == "true")
        {
            
            

                
                
               let refreshAlert = UIAlertController(title: "Update Available", message: "A new version of HalaGram is available.", preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                    
                    
                    
                    
                    let url:URL? = URL(string: "https://apps.apple.com/in/app/halagram-%D9%87%D9%84%D8%A7%D8%AC%D8%B1%D8%A7%D9%85/id1556189104")
                    
                    print(url!)
                    
                    /* First create a URL, then check whether there is an installed app that can
                     open it on the device. */
                    if UIApplication.shared.canOpenURL(url! as URL) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url! as URL, options: [:], completionHandler: { (success) in
                                
                            })
                        } else {
                            UIApplication.shared.openURL(url! as URL)
                        }
                    }
                    
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Next time", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                present(refreshAlert, animated: true, completion: nil)
                
        
//            }
//            else
//            {
//
//            }
            
            
            
            
            
        }

        

    }
    func loadActivityIndicator()
    {
if LocalizationSystem.sharedInstance.getLanguage() == "en"{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "Processing";
activityLoader?.detailsLabel.text = "Please Wait";
activityLoader?.isUserInteractionEnabled = false;
}
else{
activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
activityLoader?.label.text = "معالجة";
activityLoader?.detailsLabel.text = "فضلا النتظار";
activityLoader?.isUserInteractionEnabled = false;
}
       
    }

    func setAllView(){
//        var borderWidth = 2.0;
        
        totalLbl.makeCircular()
        
        visitorsView.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        visitorsView.cornerRadius(view: visitorsView)
        requestView.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        requestView.cornerRadius(view: requestView)
        earningsView.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        earningsView.cornerRadius(view: earningsView)
        conversionView.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        conversionView.cornerRadius(view: conversionView)
        completionView.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        completionView.cornerRadius(view: completionView)
        viewDetailsVIew.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//        viewDetailsVIew.cornerRadius(view: viewDetailsVIew)

//        visitorsView.cornerRadius(view: visitorsView)
//        visitorsView.layer.borderWidth = 1.0
//        visitorsView.layer.borderColor = UIColor.lightGray.cgColor
//        visitorsView.backgroundColor = UIColor.white
        
//        requestView.cornerRadius(view: requestView)
//        requestView.layer.borderWidth = 1.0
//        requestView.layer.borderColor = UIColor.lightGray.cgColor
//        requestView.backgroundColor = UIColor.white
//
//        earningsView.cornerRadius(view: earningsView)
//        earningsView.layer.borderWidth = 1.0
//        earningsView.layer.borderColor = UIColor.lightGray.cgColor
//        earningsView.backgroundColor = UIColor.white
//
//        conversionView.cornerRadius(view: conversionView)
//        conversionView.layer.borderWidth = 1.0
//        conversionView.layer.borderColor = UIColor.lightGray.cgColor
//        conversionView.backgroundColor = UIColor.white
//
//               completionView.cornerRadius(view: completionView)
//               completionView.layer.borderWidth = 1.0
//               completionView.layer.borderColor = UIColor.lightGray.cgColor
//               completionView.backgroundColor = UIColor.white
//
//        viewDetailsVIew.cornerRadius(view: viewDetailsVIew)
//               viewDetailsVIew.layer.borderWidth = 1.0
//               viewDetailsVIew.layer.borderColor = UIColor.lightGray.cgColor
//               viewDetailsVIew.backgroundColor = UIColor.white
        
        
    }
   
    @IBAction func menuBtnTapped(_ sender: Any) {
        if (UserDefaults.standard.bool(forKey: "slidemenu")){
            UserDefaults.standard.set(false, forKey: "slidemenu")
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
//                UserDefaults.standard.set(false, forKey: "slidemenu")
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
               
            }
            else{
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 60, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
            }
           
           
            
            
            

           
        }
       
       
        
        
    }
    @IBAction func newRequestBtn(_ sender: Any) {
        
        
        let name : UIStoryboard!
                      name = UIStoryboard(name: "Main", bundle: nil)
                      let vc = name.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController



                      DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                      }
        
        
        
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        DispatchQueue.main.async {
//             let vc = storyboard.instantiateViewController(withIdentifier: "NewRequestViewController") as! NewRequestViewController
//
//            self.navigationController?.pushViewController(vc, animated: true)
//
//
//
//
//        }
        
    }
    
    @IBAction func viewDetailsBtnTapped(_ sender: Any) {
        
        DispatchQueue.main.async {
            UIApplication.shared.open(viewdetails!)
            }
       
        
        
    }
    
    @IBAction func englishBtnTapped(_ sender: Any) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
            
        }
        else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")

//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
//            }

           


            UserDefaults.standard.setValue("arabic", forKey: "languageSetting")
            
        }
        
        
        let window = self.view.superview
        self.view.removeFromSuperview()
        window?.addSubview(self.view)
        
        viewWillAppear(true)
    }
    
}
extension UIView{
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
       layer.masksToBounds = false
       layer.shadowColor = color.cgColor
       layer.shadowOpacity = opacity
       layer.shadowOffset = offSet
       layer.shadowRadius = radius
//        layer.cornerRadius =
       layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
       layer.shouldRasterize = true
       layer.rasterizationScale = scale ? UIScreen.main.scale : 1
     }
}
