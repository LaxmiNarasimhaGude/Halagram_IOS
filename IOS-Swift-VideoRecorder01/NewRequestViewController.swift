//
//  NewRequestViewController.swift
//  IOS-Swift-VideoRecorder01
//
//  Created by Kwebmaker on 24/01/2021.
//  Copyright © 2021 Pooya Hatami. All rights reserved.
//

import UIKit
import MBProgressHUD
class NewRequestViewController: UIViewController,dataReceivedFromServerDelegate,UITableViewDelegate,UITableViewDataSource {
  
//
 
    @IBOutlet weak var topview: NSLayoutConstraint!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var noNewRequestLbl: UILabel!
    
    @IBOutlet weak var newRequestTableView: UITableView!
    @IBOutlet weak var daysRemaininLbl: UILabel!
    @IBOutlet weak var dateOfRequestLbl: UILabel!
    @IBOutlet weak var occasionLbl: UILabel!
    @IBOutlet weak var requestedByLbl: UILabel!
    
    @IBOutlet weak var daysRemainingView: UIView!
    @IBOutlet weak var viewDetailsView: UIView!
    
    var reachability = try? Reachability()
    var requestServer = RequestToServer()
    var tableviewArray: NSMutableArray?
     
    override func viewWillAppear(_ animated: Bool) {
        
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height
        topview.constant = topBarHeight
        print(topBarHeight)
        
        
        
        
        
        
        
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            noNewRequestLbl.text = "No new requests"
            headerLbl.font.fontChange(font: "DiodrumRounded-Semibold", lable: headerLbl)
        }
        else{
            noNewRequestLbl.text = "لا توجد طلبات جديدة"
            
            headerLbl.font.fontChange(font: "AvenirArabic-Heavy", lable: headerLbl)
        }
        
        
        DispatchQueue.main.async {
            self.headerLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "New Requests", comment: "")
            self.englishBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "English", comment: ""), for: .normal)
           
        }
        
        totalLbl.makeCircular()
        let totalCount = UserDefaults.standard.value(forKey: "totalCount") as! Int
        if totalCount>0{
            self.totalLbl.isHidden = false
            self.totalLbl.text = "\(totalCount)"
        }
        else{
            self.totalLbl.isHidden = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "slidemenu")

       
       requestServer.delegate = self
       
        
        if reachability!.isReachable{
            DispatchQueue.main.async {
                  self.requestServer.getDataFromServer(myGetUrl: detailsUrl!)
                self.loadActivityIndicator()
            }
                 
               }
               else{
                if LocalizationSystem.sharedInstance.getLanguage() == "en"{
                    self.alert(message: "No internet connection", title: "")
                }
                else{
                    self.alert(message: "لا يوجد اتصال بالانترنت", title: "")
                }
                          
                      }
    }
    
    // tableView delegates and Datasource methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(((tableviewArray!.object(at: indexPath.row)) as AnyObject).value(forKey: "orderId"), forKey: "orderId")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        DispatchQueue.main.async {
//            self.activityLoader?.hide(animated: true)
             let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                       
            self.navigationController?.pushViewController(vc, animated: true)
           
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0;//Choose your custom row height
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewArray!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newRequestTableView.dequeueReusableCell(withIdentifier: "NewRequestTableViewCell") as! NewRequestTableViewCell
        cell.dateOfRequestLbl.text = (((tableviewArray!.object(at: indexPath.row)) as AnyObject).value(forKey: "requestDate") as! String)
        cell.halagramOccasionLbl.text = (((tableviewArray!.object(at: indexPath.row)) as AnyObject).value(forKey: "occasion") as! String)
        cell.requestedByLbl.text = (((tableviewArray!.object(at: indexPath.row)) as AnyObject).value(forKey: "requestBy") as! String)
        cell.daysRemainingLbl.text = (((tableviewArray!.object(at: indexPath.row)) as AnyObject).value(forKey: "daysRemain") as! String)
       
        return cell
    }
    
    
    
//    func setAllViews(){
//
//        daysRemainingView.makeCircular()
//        viewDetailsView.makeCircular()
//
//    }
    func dataReceivedFromServer(data: NSDictionary, url: URL) {
             if ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "status") as! String) == "success" {
    if((data.value(forKey: "Response") as! NSDictionary).value(forKey: "message") as! String) == "no data"{
        DispatchQueue.main.async {
            activityLoader?.hide(animated: true)
            self.newRequestTableView.isHidden = true
            self.noNewRequestLbl.isHidden = false
        }
                             
    }
    else{
        let temporaryArray = ((data.value(forKey: "Response") as! NSDictionary).value(forKey: "result") as! NSMutableArray)
            print(temporaryArray)
            
            
        tableviewArray = temporaryArray.mutableCopy() as! NSMutableArray
            

                    DispatchQueue.main.async {
                        self.newRequestTableView.isHidden = false
                        self.noNewRequestLbl.isHidden = true
                        self.newRequestTableView.delegate = self
                        self.newRequestTableView.dataSource = self
                        self.newRequestTableView.reloadData()
                        activityLoader?.hide(animated: true)

            }
    }
                
          

              
                
                
                

                           
            }
             else if(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "token expired")||(((data.value(forKey: "Response") as! NSDictionary).value(forKey: "errorcode") as! String) == "invalid token"){
                
                DispatchQueue.main.async {
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

                            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

                                    let navigationController = UINavigationController(rootViewController: loginVC)



                            appDel.window?.rootViewController = navigationController
                               }

                
             }

        }
  
    
    @IBAction func newRequest(_ sender: Any) {
        
      
        
    }
    
    
    
    @IBAction func menuBtnTapped(_ sender: Any) {
        if (UserDefaults.standard.bool(forKey: "slidemenu")){
            UserDefaults.standard.set(false, forKey: "slidemenu")
            if LocalizationSystem.sharedInstance.getLanguage() == "en"{
//                UserDefaults.standard.set(false, forKey: "slidemenu")
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
               
            }
            else{
                let menuVC : SlideViewController = self.storyboard!.instantiateViewController(withIdentifier: "SlideViewController") as! SlideViewController
                         self.view.addSubview(menuVC.view)
                self.addChildViewController(menuVC)
                         menuVC.view.layoutIfNeeded()

                         menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);

                         UIView.animate(withDuration: 0.3, animations: { () -> Void in
                             menuVC.view.frame=CGRect(x: 60, y: 0, width: UIScreen.main.bounds.size.width-60, height: UIScreen.main.bounds.size.height);
                     }, completion:nil)
            }
           
           
            
            
            

           
        }
       
       
        
        
    }
    
    @IBAction func viewDetails(_ sender: Any) {
      
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         DispatchQueue.main.async {
              let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        
             self.navigationController?.pushViewController(vc, animated: true)
//             self.activityLoader?.hide(animated: true)
         }
        

        
    }
    
    @IBAction func englishBtnTapped(_ sender: Any) {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            }
            
        }
        else{
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")

//            DispatchQueue.main.async {
                UIView.appearance().semanticContentAttribute = LocalizationSystem.sharedInstance.getLanguage() == "ar" ? .forceRightToLeft : .forceLeftToRight
//            }

           


            UserDefaults.standard.setValue("arabic", forKey: "languageSetting")
            
        }
        
        
        let window = self.view.superview
        self.view.removeFromSuperview()
        window?.addSubview(self.view)
        
        viewWillAppear(true)
    }
    
    
    func loadActivityIndicator()
                   {
        if LocalizationSystem.sharedInstance.getLanguage() == "en"{
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
            activityLoader?.label.text = "Processing";
            activityLoader?.detailsLabel.text = "Please Wait";
            activityLoader?.isUserInteractionEnabled = false;
        }
        else{
            activityLoader = MBProgressHUD.showAdded(to: self.view, animated: true);
            activityLoader?.label.text = "معالجة";
            activityLoader?.detailsLabel.text = "فضلا النتظار";
            activityLoader?.isUserInteractionEnabled = false;
        }
                      
                   }
}
